      program u2fit

C     'Open' version allowing easy change of function

      implicit real*8 (a-h,o-z)
      include 'dim_open.f'
      COMMON/SAMPL1/SPECPAR(30),ANGLES(NQ),NUMSPE(NQ)
      COMMON/SAMPL2/X(NE,NQ),Y(NE,NQ),YE(NE,NQ)
      character*80 fname,ofname
      character ngfit,fitok,wt,cmx,cfa,optms,optpp,qcq
      character*20 qreg
      character*3 model
      character*6 ssmu
      logical iprop,constq
      common/filepar/ fname,ala,nsp,nch,nspo,ncho
      common/elscan/ s(ne,nq),ws(ne,nq),q2(nq),t(ne),qm2,nd1,nd2,smu,ssq,iwarn

C     List of implemented functions
C     To add a function:
C     1. Increase nfun in dim_open.f.
C     2. Add title, parameter number, and ism to the list below.
C     3. Add calculation block in function eli(q2,isfun,par).
C     4. If necessary add an initialisation block in subroutine initt, especially for global ms.
C     5. Add fit initialisation block in subroutine fit(isfun,ich,par).
C     6. If components of the fit function should be plotted add a block to subroutine plotdet.

      character*40 funcname(nfun)
      data funcname /
     &  'Gaussian',
     &  'Non-Gaussian',
     &  'Gaussian (no intensity factor)',
     &  'Non-Gaussian (no intensity factor)',
     &  'Gaussian with individual MS' ,
     &  'Diffusion',
     &  'Diffusion + fast motion',
     &  'Gaussian + sloped background',
     &  'Gaussian (no IF) + sloped BG',
     &  'Gaussian (no IF) + flat BG',
     &  'Ad hoc model with 3 parameters',
     &  'Ad hoc model with 2 parameters',
     &  'Magic function'/
      integer npar(nfun),ism(nfun)
      data npar /2,3,1,2,2,1,2,3,2,2,3,2,2/
      data ism /0,0,1,1,0,1,1,0,1,1,0,1,1/

      real*8 par(mpar,ne),dpar(mpar,ne),reg(mpar)

      write(*,'(a)') 'Fit elastic scans for <u^2>(T)'
      write(*,'(a)') "'Open' Version"
      write(*,*)

      ncho=0
      nspo=0
      write(*,'(a$)') 'Elastic scan file (SQW type) name? '
      read(*,'(a)') fname
      write(*,'(a$)') 'Constant Q? '
      read(*,'(a)') qcq
      constq=(qcq.eq."Y").or.(qcq.eq."y")
      write(*,'(a$)') 'Average first n temperatures (0:none)? '
      read(*,*) nav
      call readfile(nav,constq)
      write(*,'("Wavelength: ",f4.2)') ala
      qm2=(12.56637/ala)**2


 4    write(*,'(a$)') 'Another elastic scan file (SQW type) or ""? '
      read(*,'(a)') fname
      if (fname.eq.'') goto 1
      call readfile(0,constq)
      goto 4
 1    nch=ncho
 100  write(*,'(a$)') 'Detector range for fit? '
      read(*,*) nd1,nd2
      write(*,'(a$)') 'Highest temperature to fit (0:all)? '
      read(*,*) tmax
      if (tmax.ne.0.0) then
        do itp=2,nch
          if (t(itp).gt.tmax) goto 200
        end do
 200    nch=itp-1
      end if

      open(85,file="elscan.dat",status='unknown')
C     write(*,'(/)')
      do ich=1,nch
        write(85,'(f8.2,4x,$)') t(ich)
        do isp=1,nsp
          write(85,'(2f8.4,3x,$)') s(ich,isp),1.0/sqrt(ws(ich,isp))
        end do
        write(85,*)
      end do
      close(85)

      write(*,'(a)') 'Available Models:'
      do ifun=1,nfun
        write(*,'(i3," - ",a40,2i3)') ifun,funcname(ifun),npar(ifun),ism(ifun)
      end do
11    write(*,'(a$)') 'Select: '
      read(*,*) isfun
      if (isfun.gt.nfun) goto 11
      if (ism(isfun).gt.0) then
        write(*,'(a$)') 'Multiple scattering fraction? '
        read(*,*) smu
      end if
      write(*,*)

      write(*,'(a$)') 'Use parameter propagation (y/N)? '
      read(*,*) optpp
      iprop=((optpp.eq."y").or.(optpp.eq."Y"))

      reg=0.0
 50   write(*,'(a$)') 'Regularise (parameter number, multiplier)? '
      read(*,'(a)') qreg
      if (qreg.eq."") goto 60
      read(qreg,*) ipreg,freg
      if (ipreg.le.npar(isfun)) reg(ipreg)=freg
      goto 50
 60   continue

      ssq=0.0
      iwarn=0
      do ich=1,nch
        call fit(isfun,ich,par(1,ich),dpar(1,ich),reg,iprop)
        write(22,'(f8.2,"    ",$)') t(ich)
        do ipar=1,npar(isfun)
          write(22,'(2e12.4,"  ",$)') par(ipar,ich),dpar(ipar,ich)
        end do
        write(22,*)
      end do

      chi2=ssq/((nch-1)*(nd2-nd1+1-npar(isfun)))
      write(*,'("Final reduced chi^2: ",f8.4)') chi2

      if (ism(isfun).gt.0) then
        write(*,'(a$)') "Optimize MS fraction (y/N)? "
        read(*,*) optms
        if ((optms.eq."y").or.(optms.eq."Y")) then
          k=3
          maxf=200
          absacc=1.0d-5
          relacc=0.0
          xstep=0.1
 115      call vd01ad(k,smu,ssq,maxf,absacc,relacc,xstep)
          goto (111,112,113,114),k
 111      ssq=0.0
          do ich=1,nch
            call fit(isfun,ich,par(1,ich),dpar(1,ich),reg,iprop)
          end do
          chi2=ssq/((nch-1)*(nd2-nd1+1-npar(isfun)))
          write(*,'("MS probability: ",f6.4,"    chi^2:",f6.2)') smu,chi2
          goto 115
 112      continue
 113      continue
 114      continue
        end if
      end if

700   write(*,'(a$)') 'Plot fit result for temperature (0: end)? '
      read(*,*) ttp
      if (ttp.eq.0.0) goto 800
      do itp=1,nch
        if (t(itp).ge.ttp) goto 710
      end do
 710  call plotdet(itp,isfun,npar(isfun),par(1,itp))
      goto 700

 800  write(*,'(/a)') "How to proceed?"
      write(*,'(a)') "0: exit without saving results"
      write(*,'(a)') "1: exit saving results"
      write(*,'(a)') "2: new fit (without saving results)"
      write(*,'(a)') "3: save results and new fit"
 810  read(*,*) iact
      if (iact.eq.0) goto 900
      if (iact.eq.1) goto 850
      if (iact.eq.2) goto 100
      if (iact.eq.3) goto 850
      goto 810

 850  write(model,'(i3)') 100+isfun
      if (ism(isfun).le.0) then
        ofname="u2_m"//model(2:3)//".dat"
      else
        write(ssmu,'(f6.2)') smu
        ofname="u2_m"//model(2:3)//"_s"//ssmu(5:6)//".dat"
      end if
      open(98,file=ofname,status='unknown')

      do ich=1,nch
        write(98,'(f8.2,"    ",$)') t(ich)
        do ipar=1,npar(isfun)
          write(98,'(2e12.4,"  ",$)') par(ipar,ich),dpar(ipar,ich)
        end do
        write(98,*)
      end do
      close(98)

      if (iact.eq.3) goto 100

 900  end


      subroutine readfile(nav,constq)

      implicit real*8 (a-h,o-z)
      include 'dim_open.f'
      COMMON/SAMPL1/SPECPAR(30),ANGLES(NQ),NUMSPE(NQ)
      COMMON/SAMPL2/X(NE,NQ),Y(NE,NQ),YE(NE,NQ)
      character*80 fname
      common/filepar/ fname,alam,nsp,nch,nspo,ncho
      common/elscan/ sn(ne,nq),wsn(ne,nq),q2(nq),t(ne),qm2,nd1,nd2,smu,ssq,iwarn
      character*40 title
      dimension s0(nq),es0(nq)
      logical constq

      call crin(fname,title,ifail)
      if (ifail.ne.0) stop
      write(*,*) 'File tile: ',title
      nsp=int(specpar(4))
      nch=int(specpar(19))
      alam=specpar(21)
      write(*,130) nch,nsp
 130  format(i4,' temperatures,',i3,' detectors')
      if (nspo.ne.0) then
        if (nsp.ne.nspo) then
          write(*,*) 'Inconsistent detectors in scans!'
          stop
        end if
        goto 210
      else
        nspo=nsp
      end if
      if (nav.lt.2) then
        do ich=1,nch
           do isp=1,nsp
              if (y(ich,isp).le.0.0) goto 200
              s0(isp)=y(ich,isp)
            es0(isp)=ye(ich,isp)
           end do
           write(*,140) x(ich,1)
 140       format('Reference temperature: ',f6.2)
           goto 210
 200       continue
        end do
      else
        do isp=1,nsp
        xav=0.0
        yav=0.0
        yeav=0.0
        do ich=1,nav
          xav=xav+x(ich,isp)/ye(ich,isp)**2
          yav=yav+y(ich,isp)/ye(ich,isp)**2
          yeav=yeav+1./ye(ich,isp)**2
        end do
        s0(isp)=yav/yeav
        es0(isp)=1./sqrt(yeav)
      end do
      write(*,140) xav/yeav
      end if
 210  do isp=1,nsp
         if (constq) then
           q=angles(isp)
         else
           q=(12.5664/alam)*sin(0.00872665*angles(isp))
         end if
         q2(isp)=q*q
         if (nav.lt.2) then
           s00=s0(isp)
           es00=es0(isp)
           do ich=1,nch
              t(ich+ncho)=x(ich,isp)
              s=y(ich,isp)
              es=ye(ich,isp)
              sn(ich+ncho,isp)=s/s00
              wsn(ich+ncho,isp)=1./((es/s00)**2+(s*es00/s00**2)**2)
           end do
         else
           t(1)=xav/yeav
           sn(1,isp)=1.0
           wsn(1,isp)=1.0/es0(isp)**2
           s00=s0(isp)
           es00=es0(isp)
           do ich=2,nch
              t(ich)=x(ich+nav-1,isp)
              s=y(ich+nav-1,isp)
              es=ye(ich+nav-1,isp)
              sn(ich,isp)=s/s0(isp)
              wsn(ich+ncho,isp)=1./((es/s00)**2+(s*es00/s00**2)**2)
           end do
         end if
      end do
      if (nav.lt.2) then
        ncho=ncho+nch
      else
        ncho=ncho+nch-nav+1
      end if
      return
      end


          SUBROUTINE CRIN(FILIN,TITLIN,IFAIL)

C......... Data input of CROSSX files
C changed to semi-relaxed format

      implicit real*8 (a-h,o-z)
           include 'dim_open.f'
           COMMON/SAMPL1/SPECPAR(30),ANGLES(NQ),NUMSPE(NQ)
           COMMON/SAMPL2/X(NE,NQ),Y(NE,NQ),YE(NE,NQ)
           CHARACTER*20 FILIN
           CHARACTER*40 TITLIN
           DIMENSION NZONE(6),NCS(NQ)

4     FORMAT(8I5)
5     FORMAT(A40)
6     FORMAT(1X,F6.2,F8.3,F8.4,F9.3,F6.1,I2)
7     FORMAT(16X,3F8.4)
8     FORMAT(1X,A1)
9     FORMAT(5X,F10.5,E13.5,E12.4)
       IR=5
       IW=6
       IFAIL=0
       IFILE2=21
       OPEN (UNIT=IFILE2,FILE=FILIN,STATUS='OLD',ERR=95)

       NSPECT=0
       NCMIN=NE
       DO I=1,NQ
C       READ (IFILE2,4,END=90) NLINES,NZONE,NCHANS
        READ (IFILE2,*,END=90) NLINES,NZONE,NCHANS
        NSPECT=NSPECT+1
        NUMSPE(I)=I!This is normally to identify the detector-Cannot know where
c       the file comes from => just take the index as detector number but
c       beware of programs testing this number
        NCS(I)=NCHANS!Different spectra can have different number of channels
        IF (NCHANS.LT.NCMIN) NCMIN=NCHANS !..but we don't like that so take min
        DO J=1,6
         IF (NZONE(J).LE.0) GOTO 100
         IF (J.EQ.1) THEN
         READ (IFILE2,5) TITLIN
         ELSE
         IF (J.EQ.2) THEN
C        READ (IFILE2,6) ANGLES(I),EINC,QINC,TEMP,AMASS,ISYM
         READ (IFILE2,*) ANGLES(I),EINC,QINC,TEMP,AMASS,ISYM
C        READ (IFILE2,7)DELTAEN,DELTATAU,DELTAK
         READ (IFILE2,*)DELTAEN,DELTATAU,DELTAK
         ELSE
         IF (J.EQ.3) THEN
         READ (IFILE2,8) DATYPE
         END IF
         END IF
         END IF
100      CONTINUE
        END DO

       DO J=1,NCHANS
C       READ(IFILE2,9) X(J,I),Y(J,I),YE(J,I)
        READ(IFILE2,*) X(J,I),Y(J,I),YE(J,I)
       END DO
       END DO
90     CONTINUE
      PI=3.141592653
      SPECPAR(4)=NSPECT
      SPECPAR(19)=NCMIN  !N channels: we take the min to be sure it's filled
c                          with data
      SPECPAR(9)=TEMP
      SPECPAR(18)=DELTATAU
      IF (EINC.EQ.0.) THEN
         WRITE (IW,33)
33       FORMAT(1X,' No incident energy data found:Give it (meV)->',$)
         READ (IR,*) EINC
      END IF
      SPECPAR(21)=SQRT(81.799/EINC)

       CLOSE (UNIT=IFILE2)
       RETURN
95     IFAIL=1
       RETURN
       END


      subroutine fit(isfun,ich,par,dpar,reg,iprop)

      implicit real*8 (a-h,o-z)
      include 'dim_open.f'
      real*8 par(mpar),f(nq),w(nw),dpar(mpar),parlast(mpar),reg(mpar),reg1(mpar)
      logical iprop
      common/elscan/ s(ne,nq),ws(ne,nq),q2(nq),t(ne),qm2,nd1,nd2,smu,ssq,iwarn
      common/fitpar/ isfun1,ich1,parlast,reg1

      isfun1=isfun
      ich1=ich
      reg1=reg

      ndat=nd2-nd1+1+mpar

      if (isfun.eq.1) then
        npar=2
        if ((ich.eq.1).or.(.not.iprop)) then
          par(1)=0.0
          par(2)=1.0
        else
          par=parlast
        end if
        dstep=1.d-5
        dmax=1.0
        acc=1.d-5
        maxfun=1000
        iprint=0
      end if

      if (isfun.eq.2) then
        npar=3
        if ((ich.eq.1).or.(.not.iprop)) then
          par(1)=0.0
          par(2)=1.0
          par(3)=0.0
        else
          par=parlast
        end if
        dstep=1.d-5
        dmax=1.0
        acc=1.d-5
        maxfun=1000
        iprint=0
      end if

      if (isfun.eq.3) then
        npar=1
        if ((ich.eq.1).or.(.not.iprop)) then
          par(1)=0.0
        else
          par=parlast
        end if
        dstep=1.d-5
        dmax=1.0
        acc=1.d-5
        maxfun=1000
        iprint=0
      end if

      if (isfun.eq.4) then
        npar=2
        if ((ich.eq.1).or.(.not.iprop)) then
          par(1)=0.0
          par(2)=0.0
        else
          par=parlast
        end if
        dstep=1.d-5
        dmax=1.0
        acc=1.d-5
        maxfun=1000
        iprint=0
      end if

      if (isfun.eq.5) then
        npar=2
        if ((ich.eq.1).or.(.not.iprop)) then
          par(1)=0.0
          par(2)=0.0
        else
          par=parlast
        end if
        dstep=1.d-5
        dmax=1.0
        acc=1.d-5
        maxfun=1000
        iprint=0
      end if

      if (isfun.eq.6) then
        npar=1
        if ((ich.eq.1).or.(.not.iprop)) then
          par(1)=0.0
        else
          par=parlast
        end if
        dstep=1.d-5
        dmax=1.0
        acc=1.d-5
        maxfun=1000
        iprint=0
      end if

      if (isfun.eq.7) then
        npar=2
        if ((ich.eq.1).or.(.not.iprop)) then
          par(1)=0.0
          par(2)=0.0
        else
          par=parlast
        end if
        dstep=1.d-5
        dmax=1.0
        acc=1.d-5
        maxfun=1000
        iprint=0
      end if

      if (isfun.eq.8) then
        npar=3
        if ((ich.eq.1).or.(.not.iprop)) then
          par(1)=0.0
          par(2)=1.0
          par(3)=0.0
        else
          par=parlast
        end if
        dstep=1.d-5
        dmax=1.0
        acc=1.d-5
        maxfun=1000
        iprint=0
      end if

      if (isfun.eq.9) then
        npar=2
        if ((ich.eq.1).or.(.not.iprop)) then
          par(1)=0.0
          par(2)=0.0
        else
          par=parlast
        end if
        dstep=1.d-5
        dmax=1.0
        acc=1.d-5
        maxfun=1000
        iprint=0
      end if

      if (isfun.eq.10) then
        npar=2
        if ((ich.eq.1).or.(.not.iprop)) then
          par(1)=0.0
          par(2)=0.3
        else
          par=parlast
        end if
        dstep=1.d-5
        dmax=1.0
        acc=1.d-5
        maxfun=1000
        iprint=0
      end if

      if (isfun.eq.11) then
        npar=3
        if ((ich.eq.1).or.(.not.iprop)) then
          par(1)=0.0
          par(2)=1.0
          par(3)=0.1
        else
          par=parlast
        end if
        dstep=1.d-5
        dmax=1.0
        acc=1.d-5
        maxfun=1000
        iprint=0
      end if

      if (isfun.eq.12) then
        npar=2
        if ((ich.eq.1).or.(.not.iprop)) then
          par(1)=0.0
          par(2)=0.0
        else
          par=parlast
        end if
        dstep=1.d-5
        dmax=1.0
        acc=1.d-7
        maxfun=1000
        iprint=1
      end if

      if (isfun.eq.13) then
        npar=2
        if ((ich.eq.1).or.(.not.iprop)) then
          par(1)=0.0
          par(2)=0.0
        else
          par=parlast
        end if
        dstep=1.d-5
        dmax=1.0
        acc=1.d-7
        maxfun=1000
        iprint=1
      end if

      call va05ad(ndat,npar,f,par,dstep,dmax,acc,maxfun,iprint,w)

      ssq1=0.0
      do j=1,ndat
        ssq1=ssq1+f(j)**2
      end do
      write(99,*) t(ich),ssq1
      ssq=ssq+ssq1

      parlast=par

      call kova(q2(nd1:nd2),s(ich,nd1:nd2),ws(ich,nd1:nd2),par,dpar,dstep,ndat,npar,1,nr)
      if (nr.ne.0) then
        dpar=0.0
        if (iwarn.eq.0) then
          write(*,'(a)') "Error in KOVA: some error bars will be missing!"
          iwarn=nr
        end if
      end if

      return
      end


      subroutine calfun(m,n,f,par)

      implicit real*8 (a-h,o-z)
      include 'dim_open.f'
      real*8 par(mpar),f(nq),fct(250),parlast(mpar),reg(mpar)
      common/elscan/ s(ne,nq),ws(ne,nq),q2(nq),t(ne),qm2,nd1,nd2,smu,ssq,iwarn
      common/fitpar/ isfun,ich,parlast,reg
      common/funpar/ par1(mpar),elis
C     This common is for subroutine kova:
      common/ff/ fct

      call initt(par)

      j=1
      do iq=nd1,nd2
        y=eli(q2(iq),isfun,par)
        fct(j)=y
        f(j)=(s(ich,iq)-y)*sqrt(ws(ich,iq))
        j=j+1
      end do

      if (ich.gt.1) then
        do ipar=1,mpar
          f(j)=reg(ipar)*(par(ipar)-parlast(ipar))
          j=j+1
        end do
      else
       do ipar=1,mpar
          f(j)=0.0
          j=j+1
        end do
      end if

      return
      end


      subroutine initt(par)

      implicit real*8 (a-h,o-z)
      include 'dim_open.f'
      real*8 par(mpar),parlast(mpar),reg(mpar)
      common/elscan/ s(ne,nq),ws(ne,nq),q2(nq),t(ne),qm2,nd1,nd2,smu,ssq,iwarn
      common/fitpar/ isfun,ich,parlast,reg
      common/funpar/ par1(mpar),elis
      external fngauss,fdiff,fdifff,fadhoc,fmagic

      par1=par

C     Calculations which have to be done only once for each temperature
C     go here and place their results in common/funpar/:

      if (isfun.eq.4) then
        call qa05ad(aiel,fngauss,0.0d0,qm2,1.0d-5,0.0d0,1,error,iflag)
        elis=aiel/qm2
      end if

      if (isfun.eq.6) then
        call qa05ad(aiel,fdiff,0.0d0,qm2,1.0d-5,0.0d0,1,error,iflag)
        elis=aiel/qm2
      end if

      if (isfun.eq.7) then
        call qa05ad(aiel,fdifff,0.0d0,qm2,1.0d-5,0.0d0,1,error,iflag)
        elis=aiel/qm2
      end if

      if (isfun.eq.12) then
        call qa05ad(aiel,fadhoc,0.0d0,qm2,1.0d-5,0.0d0,1,error,iflag)
        elis=aiel/qm2
      end if

      if (isfun.eq.13) then
        call qa05ad(aiel,fmagic,0.0d0,qm2,1.0d-5,0.0d0,1,error,iflag)
        elis=aiel/qm2
      end if

      return
      end


      real*8 function eli(q2,isfun,par)

      implicit real*8 (a-h,o-z)
      include 'dim_open.f'
      real*8 par(mpar)
      common/elscan/ s(ne,nq),ws(ne,nq),dummy(nq),t(ne),qm2,nd1,nd2,smu,ssq,iwarn
      common/funpar/ par1(mpar),elis

      if (isfun.eq.1) then
        eli = par(2)*exp(-0.3333333*par(1)*q2)
      end if

      if (isfun.eq.2) then
        eli = par(2)*exp(-0.3333333*par(1)*q2+q2**2*par(3)*par(2)**2/18.0)
      end if

      if (isfun.eq.3) then
        if (smu.eq.0.0) then
          eli = exp(-0.3333333*par(1)*q2)
        else
          if (par(1).eq.0.0) then
            eli = 1.0
          else
            eli0 = exp(-0.3333333*par(1)*q2)
            elis = 3.0/(par(1)*qm2)*(1.0-exp(-0.3333333*par(1)*qm2))
            eli = (1.0-smu)*(eli0+smu*elis**2/(1.0-smu*elis))
          end if
        end if
      end if

      if (isfun.eq.4) then
        if (smu.eq.0.0) then
          eli = fngauss(q2)
        else
          eli0 = fngauss(q2)
          eli = (1.0-smu)*(eli0+smu*elis**2/(1.0-smu*elis))
        end if
C       write(*,*) eli0,eli
      end if

      if (isfun.eq.5) then
        if (par(1).eq.0.0) then
          eli = 1.0
        else
          eli0 = exp(-0.3333333*par(1)*q2)
          elis = 3.0/(par(1)*qm2)*(1.0-exp(-0.3333333*par(1)*qm2))
          eli = (1.0-par(2))*(eli0+par(2)*elis**2/(1.0-par(2)*elis))
        end if
      end if

      if (isfun.eq.6) then
        if (smu.eq.0.0) then
          eli = fdiff(q2)
        else
          eli0 = fdiff(q2)
          eli = (1.0-smu)*(eli0+smu*elis**2/(1.0-smu*elis))
        end if
      end if

      if (isfun.eq.7) then
        if (smu.eq.0.0) then
          eli = fdifff(q2)
        else
          eli0 = fdifff(q2)
          eli = (1.0-smu)*(eli0+smu*elis**2/(1.0-smu*elis))
        end if
      end if

      if (isfun.eq.8) then
        eli = par(2)*exp(-0.3333333*par(1)*q2)+par(3)*(qm2-q2)
      end if

      if (isfun.eq.9) then
        if (smu.eq.0.0) then
          eli = exp(-0.3333333*par(1)*q2)+par(2)*(qm2-q2)
        else
          if (par(1).eq.0.0) then
            eli = 1.0+par(2)*(qm2-q2)
          else
            eli0 = exp(-0.3333333*par(1)*q2)
            elis = 3.0/(par(1)*qm2)*(1.0-exp(-0.3333333*par(1)*qm2))
            eli = (1.0-smu)*(eli0+smu*elis**2/(1.0-smu*elis))+par(2)*(qm2-q2)
          end if
        end if
      end if

      if (isfun.eq.10) then
        if (smu.eq.0.0) then
          eli = exp(-0.3333333*par(1)*q2)+par(2)
        else
          if (par(1).eq.0.0) then
            eli = 1.0+par(2)
          else
            eli0 = exp(-0.3333333*par(1)*q2)
            elis = 3.0/(par(1)*qm2)*(1.0-exp(-0.3333333*par(1)*qm2))
            eli = (1.0-smu)*(eli0+smu*elis**2/(1.0-smu*elis))+par(2)
          end if
        end if
      end if

      if (isfun.eq.11) then

C       eli = par(2)*(exp(-0.3333333*par(1)*q2)+par(3))

C       a1=par(3)
C       a3=log(par(2))-a1
C       a2=par(1)/(3*par(2)*a1)
C       eli = exp(a1*exp(-a2*q2)+a3)

C       eli = exp (par(2)/(par(1)+q2)+par(3))

C       a1=2/(par(2)*par(3)-1.0)
C       a2=par(1)*par(3)/(3.0*(a1+1.0))
C       a3=log(par(2))-a1
C       eli= par(2) * exp( 3.0*par(1)*par(3)**2/(par(1)*q2+3.0*par(1)*par(3)) - par(3) )

        Delta=2.0/par(3)
        a=0.1666666667*par(1)*par(3)
        eli = par(2)*exp(Delta*(1.0/(1.0+a*q2)-1.0))

      end if

      if (isfun.eq.12) then
        if (smu.eq.0.0) then
          eli = fadhoc(q2)
        else
          eli0 = fadhoc(q2)
          eli = (1.0-smu)*(eli0+smu*elis**2/(1.0-smu*elis))
        end if
      end if

      if (isfun.eq.13) then
        if (smu.eq.0.0) then
          eli = fmagic(q2)
        else
          eli0 = fmagic(q2)
          eli = (1.0-smu)*(eli0+smu*elis**2/(1.0-smu*elis))
C         write(*,*) eli0,elis,eli
        end if
      end if

      return
      end


      real*8 function fngauss(q2)

      implicit real*8 (a-h,o-z)
      include 'dim_open.f'
      common/funpar/ par(mpar),elis

      fngauss=exp(-0.3333333*par(1)*q2+0.05555556*q2**2*par(2)*par(1)**2)

      return
      end


      real*8 function fdiff(q2)

      implicit real*8 (a-h,o-z)
      include 'dim_open.f'
      common/funpar/ par(mpar),elis

      fdifff = erfc_scaled(2.9540897515e-1*par(1)*q2)

      return
      end


      real*8 function fdifff(q2)

      implicit real*8 (a-h,o-z)
      include 'dim_open.f'
      common/funpar/ par(mpar),elis

      u2diff=par(1)-par(2)
      fdifff = exp(-0.3333333*par(1)*q2) * erfc_scaled(2.9540897515e-1*u2diff*q2)

      return
      end


      real*8 function fadhoc(q2)

      implicit real*8 (a-h,o-z)
      include 'dim_open.f'
      common/funpar/ par(mpar),elis

      if (par(2).eq.0.0) then
        fadhoc=exp(-0.3333333333*par(1)*q2)
      else
        a2=1.0/(1.0/par(2)**2+0.2)
        Delta=2.0/a2
        a=0.1666666667*par(1)*a2
        fadhoc=exp(Delta*(1.0/(1.0+a*q2)-1.0))
      end if
C     write(21,*) par(1),par(2),fadhoc

C     Delta=2.0/(1.0+par(2))
C     a=0.1666666667*par(1)*(1.0+par(2))
C     fadhoc=1.0-Delta+Delta/(1.0+a*q2)

C     fadhoc=exp(-0.3333333*par(1)*q2+par(2)*q2**2)

      return
      end


      real*8 function fmagic(q2)

      implicit real*8 (a-h,o-z)
      include 'dim_open.f'
      common/funpar/ par(mpar),elis

      if (par(2).eq.0.0) then
        fmagic=exp(-0.3333333333*par(1)*q2)
      else
        Delta=2.0/par(2)
        a=0.1666666667*par(1)*par(2)
        fmagic=exp(Delta*(1.0/(1.0+a*q2)-1.0))
      end if

      return
      end


      subroutine plotdet(itp,isfun,npar,par)

      implicit real*8 (a-h,o-z)
      include 'dim_open.f'
      character*80 fname
      common/filepar/ fname,ala,nsp,nch,nspo,ncho
      common/elscan/ s(ne,nq),ws(ne,nq),q2(nq),t(ne),qm2,nd1,nd2,smu,ssq,iwarn
      common/funpar/ par1(mpar),elis
      parameter(npl=50)
      dimension x(nq),y(nq),e(nq),xf(npl),yf(npl)
      character*6 temp
      real*8 par(mpar)

      do isp=1,nsp
         x(isp)=q2(isp)
         y(isp)=s(itp,isp)
         e(isp)=1./sqrt(ws(itp,isp))
      end do

      call initt(par)
      xmax=qm2
      dx=xmax/(npl-1)
      do ix=1,npl
         xf(ix)=(ix-1)*dx
         yf(ix)=eli(xf(ix),isfun,par)
      end do

      open(30,file='u2_data.dat',status='unknown')
      open(33,file='u2_igdata.dat',status='unknown')
      do i=1,nsp
        if ((i.ge.nd1).and.(i.le.nd2)) then
          write(30,*) x(i),y(i),e(i)
      else
        write(33,*) x(i),y(i),e(i)
      end if
      end do
      close(30)
      close(33)

      open(31,file='u2_fit.dat',status='unknown')
      do i=1,npl
        write(31,*) xf(i),yf(i)
      end do
      close(31)

      open(32,file='u2_gpl.inp',status='unknown')

      write(32,*) 'set logscale y'
      write(32,'(a,i0,a)') "set title 'Elastic Scan Record #",itp,"'"
      write(32,'(a)') "set xlabel 'Q^2'"
      write(32,'(a)') "set ylabel 'I_{el} / I_{el}(T=0)'"
      write(32,'(a,f8.4,$)') 'set label "<u^2>:',par(1)
      do ipar=2,npar
        write(32,'(a,i0,":",e12.4,$)') "\nparameter ",ipar,par(ipar)
      end do
      if (elis.ne.0.0) then
        write(32,'(a,f8.4,$)') '\nMultiple Scattering Fraction: ',smu
      end if
      write(32,'(a)') '" at graph 0.02, graph 0.95'

      write(32,'("plot [",e12.4,":",e12.4,"] [:2] \")') 0.0,qm2
      write(temp,'(f6.2)') t(itp)
      write(32,*)
     &  '"u2_data.dat" with errorbars lt 1 title "T =',temp,' ", \'
      if ((nd1.gt.1).or.(nd2.lt.nsp)) then

      write(32,*)
     &    '"u2_igdata.dat" with errorbars lt 4 title "ignored", \'
      end if

      write(32,*) '"u2_fit.dat" with lines lt 2 title "Fit"'

      if (isfun.eq.5) then
        elis = 3.0/(par(1)*qm2)*(1.0-exp(-0.3333333*par(1)*qm2))
C       write(*,*) elis,par(1)
        call plotmsc((1.0-par(2))*(par(2)*elis**2/(1.0-par(2)*elis)))
        goto 100
      end if

      if (isfun.eq.8) then
        open(35,file='u2_comp.dat',status='unknown')
        do ix=npl/10,npl-1
           xc=ix*dx
           yc=par(3)*(qm2-xc)
          write(35,*) xc,yc
        end do
        close(35)
        write(32,'(a)') "replot 'u2_comp.dat' with lines lt 5 title 'Background'"
      end if

      if (isfun.eq.9) then
        open(35,file='u2_comp.dat',status='unknown')
        do ix=npl/10,npl-1
           xc=ix*dx
           yc=par(2)*(qm2-xc)
          write(35,*) xc,yc
        end do
        close(35)
        write(32,'(a)') "replot 'u2_comp.dat' with lines lt 5 title 'Background'"
      end if

      if (isfun.eq.10) then
        open(35,file='u2_comp.dat',status='unknown')
        do ix=npl/10,npl-1
           xc=ix*dx
           yc=par(2)
          write(35,*) xc,yc
        end do
        close(35)
        write(32,'(a)') "replot 'u2_comp.dat' with lines lt 5 title 'Background'"
      end if

      if (elis.ne.0.0) then
        call plotmsc((1.0-smu)*(smu*elis**2/(1.0-smu*elis)))
      end if

100   continue

      close(32)

      call system('/usr/local/bin/gnuplot -persist u2_gpl.inp')

      return
      end


      subroutine plotmsc(emsc)

      implicit real*8 (a-h,o-z)
      include 'dim_open.f'
      character*80 fname
      common/elscan/ s(ne,nq),ws(ne,nq),q2(nq),t(ne),qm2,nd1,nd2,smu,ssq,iwarn

      write(32,'(a)') "replot '-' with lines lt 3 title 'Multiple Scattering'"
      write(32,*) 0.0,emsc
      write(32,*) qm2,emsc
      write(32,'(a)') "EOF"

      return
      end



C*********************************************************************
C**************  UNTERPROGRAMM  va05ad.hsl  **************************

C HSL VERSION CONTROL RECORD
C va05ad.f 11.1 93/05/06
C######DATE 14 MAR 1990     COPYRIGHT UKAEA, HARWELL.
C######ALIAS VA05AD
C###### CALLS   MB11
      SUBROUTINE VA05AD(M,N,F,X,DSTEP,DMAX,ACC,MAXFUN,IPRINT,W)
      DOUBLE PRECISION ACC,AD,ANMULT,AP,DD,DM,DMAX,DMULT,DN,DPAR,DS
      DOUBLE PRECISION DSS,DSTEP,DTEST,DW,F,FMIN,FNP,FSQ,PAR,PARM
      DOUBLE PRECISION PJ,PPAR,PRED,PTM,SP,SPP,SS,ST,TINC,W,X
C     NOTE THAT THE INSTRUCTION CALLING SUBROUTINE 'MB11A',
C      ON LINE NUMBER '138' ,IS NOT STANDARD FORTRAN
      DIMENSION F(1),X(1),W(1)
C     SET VARIOUS PARAMETERS
      MAXC=0
C     'MAXC' COUNTS THE NUMBER OF CALLS OF CALFUN
      MPN=M+N
      NT=N+2
      NTEST=0
C     'NT' AND 'NTEST' CAUSE AN ERROR RETURN IF F(X) DOES NOT DECREASE
      DTEST=FLOAT(N+N)-0.5
C     'DTEST' IS USED IN A TEST TO MAINTAIN LINEAR INDEPENDENCE
C     PARTITION THE WORKING SPACE ARRAY W
C     THE FIRST PARTITION HOLDS THE JACOBIAN APPROXIMATION
      NWI=M*N
C     THE NEXT PARTITION HOLDS THE GENERALIZED INVERSE
      NWX=NWI+MPN*N
C     THE NEXT PARTITION HOLDS THE BEST VECTOR X
      NWF=NWX+N
C     THE NEXT PARTITION HOLDS THE BEST VECTOR F
      NWC=NWF+M
C     THE NEXT PARTITION HOLDS THE COUNTS OF THE INDEPENDENT DIRECTIONS
      NWD=NWC+N
C     THE NEXT PARTITION HOLDS THE INDEPENDENT DIRECTIONS
      NWW=NWD+N*N
C     THE REMAINDER OF W IS USED FOR SCRATCH VECTORS
      NWV=NWW+N
      NWT=NWV+M
      NWU=NWT+N
      FMIN=-1.0D0
C     USUALLY 'FMIN' IS THE LEAST CALCULATED VALUE OF F(X)
      DD=0.0D0
C     USUALLY 'DD' IS THE SQUARE OF THE CURRENT STEP LENGTH
      DSS=DSTEP*DSTEP
      DM=DMAX*DMAX
      PARM=DSQRT(ACC)/DMAX
C     'PARM' IS THE LEAST VALUE OF THE MARQUARDT PARAMETER
      DPAR=10.0D0*DM
C     'DPAR' AND 'NTPAR' ARE USED TO REGULATE THE MARQUARDT PARAMETER
      IS=4
C     'IS' CONTROLS A GO TO STATEMENT FOLLOWING A CALL OF CALFUN
      IC=0
      TINC=1.0D0
C     'TINC' IS USED IN THE CRITERION TO INCREASE THE STEP LENGTH
C     START A NEW PAGE FOR PRINTING
      IF (IPRINT) 1,3,1
    1 WRITE(21,2)
    2 FORMAT (1H1,4X,'THE FOLLOWING OUTPUT IS PROVIDED BY SUBROUTINE',
     1' VA05AD'//)
      IPC=0
      GO TO 3
C     TEST WHETHER THERE HAVE BEEN MAXFUN CALLS OF CALFUN
    4 IF (MAXFUN-MAXC) 5,5,3
    5 IF (IPRINT) 139,140,139
  140 IPRINT=2
      GO TO 19
  139 WRITE(21,6)MAXC
    6 FORMAT (///5X,'ERROR RETURN FROM VA05A BECAUSE THERE HAVE BEEN',
     1I5,' CALLS OF CALFUN')
      GO TO 7
C     CALL THE SUBROUTINE CALFUN
    3 MAXC=MAXC+1
      CALL CALFUN (M,N,F,X)
C     CALCULATE THE SUM OF SQUARES
      FSQ=0.0D0
      DO 8 I=1,M
      FSQ=FSQ+F(I)*F(I)
    8 CONTINUE
C     TEST FOR ERROR RETURN BECAUSE F(X) DOES NOT DECREASE
      GO TO (9,10,9,10),IS
    9 IF (FSQ-FMIN) 11,12,12
   12 IF (DD-DSS) 13,13,10
   13 NTEST=NTEST-1
      IF (NTEST) 14,14,10
   14 IF (IPRINT) 15,17,15
   17 IPRINT=1
      GO TO 19
   15 WRITE(21,16)
   16 FORMAT (///5X,'ERROR RETURN FROM VA05A BECAUSE F(X) NO LONGER',
     1' DECREASES'//5X,'THIS MAY BE DUE TO THE VALUES OF DSTEP',
     2' AND ACC, OR TO LOSS OF RANK IN THE JACOBIAN MATRIX')
C     PROVIDE PRINTING OF FINAL SOLUTION IF REQUESTED
    7 IF (IPRINT) 18,19,18
   18 WRITE(21,20)MAXC
   20 FORMAT (///5X,'THE FINAL SOLUTION CALCULATED BY VA05A REQUIRED',
     1I5,' CALLS OF CALFUN, AND IS')
      WRITE(21,21)(I,W(NWX+I),I=1,N)
   21 FORMAT (//4X,'I',7X,'X(I)',10X,'I',7X,'X(I)',10X,'I',7X,'X(I)',
     110X,'I',7X,'X(I)',10X,'I',7X,'X(I)'//5(I5,D17.8))
c     WRITE(6,22)(I,W(NWF+I),I=1,M)
   22 FORMAT (//4X,'I',7X,'F(I)',10X,'I',7X,'F(I)',10X,'I',7X,'F(I)',
     110X,'I',7X,'F(I)',10X,'I',7X,'F(I)'//5(I5,D17.8))
      WRITE(21,23)FMIN
   23 FORMAT (/5X,'THE SUM OF SQUARES IS',D17.8)
C     RESTORE THE BEST VALUES OF X AND F
   19 DO 135 I=1,N
      X(I)=W(NWX+I)
  135 CONTINUE
      DO 136 I=1,M
      F(I)=W(NWF+I)
  136 CONTINUE
      RETURN
   11 NTEST=NT
C     PROVIDE ORDINARY PRINTING IF REQUESTED
   10 IF(IABS(IPRINT)-1) 39,38,40
   38 WRITE(21,41)MAXC
   41 FORMAT (///5X,'AT THE',I5,' TH CALL OF CALFUN WE HAVE')
   42 WRITE(21,21)(I,X(I),I=1,N)
      WRITE(21,23)FSQ
      IF (IPRINT) 39,39,142
  142 WRITE(21,22) (I,F(I),I=1,M)
      GO TO 39
   40 IPC=IPC-1
      IF (IPC) 43,43,39
   43 WRITE(21,44)MAXC
   44 FORMAT (///5X,'THE BEST ESTIMATE AFTER',I5,' CALLS OF CALFUN IS')
      IPC=IABS(IPRINT)
      IF (FSQ-FMIN) 42,45,45
   45 IF (FMIN) 42,46,46
   46 WRITE(21,21)(I,W(NWX+I),I=1,N)
      WRITE(21,23)FMIN
      IF (IPRINT) 39,39,143
  143 WRITE(21,22) (I,W(NWF+I),I=1,M)
   39 GO TO (49,47,47,48),IS
C     STORE THE INITIAL VECTORS X AND F
   48 IF (IC) 50,50,51
   50 DO 52 I=1,N
      W(NWX+I)=X(I)
   52 CONTINUE
      GO TO 54
C     CALCULATE THE INITIAL JACOBIAN APPROXIMATION
   51 K=IC
      DO 55 I=1,M
      W(K)=(F(I)-W(NWF+I))/DSTEP
      K=K+N
   55 CONTINUE
C     TEST WHETHER THE MOST RECENT X IS BEST
      IF (FMIN-FSQ) 56,56,57
   56 X(IC)=W(NWX+IC)
      GO TO 58
   57 W(NWX+IC)=X(IC)
   54 DO 53 I=1,M
      W(NWF+I)=F(I)
   53 CONTINUE
      FMIN=FSQ
C     SET X FOR THE NEXT CALL OF CALFUN
   58 IC=IC+1
      IF (IC-N) 59,59,60
   59 X(IC)=W(NWX+IC)+DSTEP
      GO TO 3
C     SET THE DIRECTION MATRIX
   60 K=NWD
      DO 61 I=1,N
      DO 62 J=1,N
      K=K+1
      W(K)=0.0D0
   62 CONTINUE
      W(K+I-N)=1.D0
      W(NWC+I)=1.0D0+DFLOAT(N-I)
   61 CONTINUE
C     SET THE MARQUARDT PARAMETER TO ITS LEAST VALUE
   24 PAR=PARM
C     COPY THE JACOBIAN AND APPEND THE MARQUARDT MATRIX
   25 PPAR=PAR*PAR
      NTPAR=0
   63 KK=0
      K=NWI+NWI
      DO 26 I=1,N
      DO 141 J=1,M
      KK=KK+1
      W(KK+NWI)=W(KK)
  141 CONTINUE
      DO 27 J=1,N
      K=K+1
      W(K)=0.0D0
   27 CONTINUE
      W(K+I-N)=PAR
   26 CONTINUE
C     CALCULATE THE GENERALIZED INVERSE OF J
      CALL MB11AD(N,MPN,W(NWI+1),N,W(NWW+1))
C     NOTE THAT THE THIRD AND FIFTH ENTRIES OF THIS ARGUMENT LIST
C     STAND FOR ONE-DIMENSIONAL ARRAYS.
C     START THE ITERATION BY TESTING FMIN
   64 IF (FMIN-ACC) 7,7,65
C     NEXT PREDICT THE DESCENT AND MARQUARDT MINIMA
   65 DS=0.0D0
      DN=0.0D0
      SP=0.0D0
      DO 66 I=1,N
      X(I)=0.0D0
      F(I)=0.0D0
      K=I
      DO 67 J=1,M
      X(I)=X(I)-W(K)*W(NWF+J)
      F(I)=F(I)-W(NWI+K)*W(NWF+J)
      K=K+N
   67 CONTINUE
      DS=DS+X(I)*X(I)
      DN=DN+F(I)*F(I)
      SP=SP+X(I)*F(I)
   66 CONTINUE
C     PREDICT THE REDUCTION IN F(X) DUE TO THE MARQUARDT STEP
C     AND ALSO PREDICT THE LENGTH OF THE STEEPEST DESCENT STEP
      PRED=SP+SP
      DMULT=0.0D0
      K=0
      DO 68 I=1,M
      AP=0.0D0
      AD=0.0D0
      DO 69 J=1,N
      K=K+1
      AP=AP+W(K)*F(J)
      AD=AD+W(K)*X(J)
   69 CONTINUE
      PRED=PRED-AP*AP
      DMULT=DMULT+AD*AD
   68 CONTINUE
C     TEST FOR CONVERGENCE
      IF (DN-DM) 28,28,29
   28 AP=DSQRT(DN)
      IF(PRED+2.0D0*PPAR*AP*(DMAX-AP)-ACC)7,7,70
   29 IF (PRED+PPAR*(DM-DN)-ACC) 7,7,70
C     TEST WHETHER TO APPLY THE FULL MARQUARDT CORRECTION
   70 DMULT=DS/DMULT
      DS=DS*DMULT*DMULT
   71 IS=2
      IF (DN-DD) 72,72,73
C     TEST THAT THE MARQUARDT PARAMETER HAS ITS LEAST VALUE
   72 IF (PAR-PARM) 30,30,24
   30 DD=DMAX1(DN,DSS)
      DS=0.25D0*DN
      TINC=1.0D0
      IF (DN-DSS) 74,132,132
   74 IS=3
      GO TO 103
C     TEST WHETHER TO INCREASE THE MARQUARDT PARAMETER
   73 IF (DN-DPAR) 31,31,32
   31 NTPAR=0
      GO TO 33
   32 IF (NTPAR) 34,34,35
   34 NTPAR=1
      PTM=DN
      GO TO 33
   35 NTPAR=NTPAR+1
      PTM=DMIN1(PTM,DN)
      IF (NTPAR-NT) 33,36,36
C     SET THE LARGER VALUE OF THE MARQUARDT PARAMETER
   36 PAR=PAR*(PTM/DM)**0.25D0
      IF(6.0D0*DD-DM)137,25,25
  137 AP=DSQRT(PRED/DN)
      IF (AP-PAR) 25,25,138
  138 PAR=DMIN1(AP,PAR*(DM/(6.0D0*DD))**0.25D0)
      GO TO 25
C     TEST WHETHER TO USE THE STEEPEST DESCENT DIRECTION
   33 IF (DS-DD) 75,76,76
C     TEST WHETHER THE INITIAL VALUE OF DD HAS BEEN SET
   76 IF (DD) 77,77,78
   77 DD=DMIN1(DM,DS)
      IF (DD-DSS) 79,78,78
   79 DD=DSS
      GO TO 71
C     SET THE MULTIPLIER OF THE STEEPEST DESCENT DIRECTION
   78 ANMULT=0.D0
      DMULT=DMULT*DSQRT(DD/DS)
      GO TO 80
C     INTERPOLATE BETWEEN THE STEEPEST DESCENT AND MARQUARDT DIRECTIONS
   75 SP=SP*DMULT
      ANMULT=(DD-DS)/((SP-DS)+DSQRT((SP-DD)**2+(DN-DD)*(DD-DS)))
      DMULT=DMULT*(1.0D0-ANMULT)
C     CALCULATE THE CORRECTION TO X, AND ITS ANGLE WITH THE FIRST
C     DIRECTION
   80 DN=0.0D0
      SP=0.0D0
      DO 81 I=1,N
      F(I)=DMULT*X(I)+ANMULT*F(I)
      DN=DN+F(I)*F(I)
      SP=SP+F(I)*W(NWD+I)
   81 CONTINUE
      DS=0.25D0*DN
C     TEST WHETHER AN EXTRA STEP IS NEEDED FOR INDEPENDENCE
      IF (W(NWC+1)-DTEST) 132,132,82
   82 IF (SP*SP-DS) 83,132,132
C     TAKE THE EXTRA STEP AND UPDATE THE DIRECTION MATRIX
   83 DO 84 I=1,N
      X(I)=W(NWX+I)+DSTEP*W(NWD+I)
      W(NWC+I)=W(NWC+I+1)+1.D0
   84 CONTINUE
      W(NWD)=1.D0
      IF(N.LE.1)GO TO 4
      DO 85 I=1,N
      K=NWD+I
      SP=W(K)
      DO 86 J=2,N
      W(K)=W(K+N)
      K=K+N
   86 CONTINUE
      W(K)=SP
   85 CONTINUE
      GO TO 4
C     EXPRESS THE NEW DIRECTION IN TERMS OF THOSE OF THE DIRECTION
C     MATRIX, AND UPDATE THE COUNTS IN W(NWC+1) ETC.
  132 IF(N.GE.2)GO TO 153
      IS=1
      GO TO 152
  153 SP=0D0
      K=NWD
      DW=0.0D0
      DO 87 I=1,N
      X(I)=DW
      DW=0.0D0
      DO 88 J=1,N
      K=K+1
      DW=DW+F(J)*W(K)
   88 CONTINUE
      GO TO (89,90),IS
   90 W(NWC+I)=W(NWC+I)+1.D0
      SP=SP+DW*DW
      IF (SP-DS) 87,87,91
   91 IS=1
      KK=I
      X(1)=DW
      GO TO 92
   89 X(I)=DW
   92 W(NWC+I)=W(NWC+I+1)+1.D0
   87 CONTINUE
      W(NWD)=1.D0
C     REORDER THE DIRECTIONS SO THAT KK IS FIRST
      IF (KK-1) 93,93,94
   94 KS=NWC+KK*N
      DO 95 I=1,N
      K=KS+I
      SP=W(K)
      DO 96 J=2,KK
      W(K)=W(K-N)
      K=K-N
   96 CONTINUE
      W(K)=SP
   95 CONTINUE
C     GENERATE THE NEW ORTHOGONAL DIRECTION MATRIX
   93 DO 97 I=1,N
      W(NWW+I)=0.D0
   97 CONTINUE
      SP=X(1)*X(1)
      K=NWD
      DO 98 I=2,N
       DS=DSQRT(SP*(SP+X(I)*X(I)))
      DW=SP/DS
      DS=X(I)/DS
      SP=SP+X(I)*X(I)
      DO 99 J=1,N
      K=K+1
      W(NWW+J)=W(NWW+J)+X(I-1)*W(K)
      W(K)=DW*W(K+N)-DS*W(NWW+J)
   99 CONTINUE
   98 CONTINUE
      SP=1.0D0/DSQRT(DN)
      DO 100 I=1,N
      K=K+1
      W(K)=SP*F(I)
  100 CONTINUE
C     PREDICT THE NEW RIGHT HAND SIDES
  152 FNP=0.0D0
      K=0
      DO 101 I=1,M
      W(NWW+I)=W(NWF+I)
      DO 102 J=1,N
      K=K+1
      W(NWW+I)=W(NWW+I)+W(K)*F(J)
  102 CONTINUE
      FNP=FNP+W(NWW+I)**2
  101 CONTINUE
C     CALCULATE THE NEXT VECTOR X, AND THEN CALL CALFUN
  103 DO 104 I=1,N
      X(I)=W(NWX+I)+F(I)
  104 CONTINUE
      GO TO 4
C     UPDATE THE STEP SIZE
   49 DMULT=0.9D0*FMIN+0.1D0*FNP-FSQ
      IF (DMULT) 105,108,108
  105 DD=DMAX1(DSS,0.25D0*DD)
       TINC=1.0D0
      IF (FSQ-FMIN) 106,107,107
C     TRY THE TEST TO DECIDE WHETHER TO INCREASE THE STEP LENGTH
  108 SP=0.0D0
      SS=0.0D0
      DO 109 I=1,M
      SP=SP+DABS(F(I)*(F(I)-W(NWW+I)))
      SS=SS+(F(I)-W(NWW+I))**2
  109 CONTINUE
      PJ=1.0D0+DMULT/(SP+DSQRT(SP*SP+DMULT*SS))
      SP=DMIN1(4.0D0,TINC,PJ)
      TINC=PJ/SP
      DD=DMIN1(DM,SP*DD)
      GO TO 106
C     IF F(X) IMPROVES STORE THE NEW VALUE OF X
   47 IF (FSQ-FMIN) 106,110,110
  106 FMIN=FSQ
      DO 111 I=1,N
      SP=X(I)
      X(I)=W(NWX+I)
      W(NWX+I)=SP
  111 CONTINUE
      DO 112 I=1,M
      SP=F(I)
      F(I)=W(NWF+I)
      W(NWF+I)=SP
  112 CONTINUE
  110 GO TO (107,107,113),IS
  113 IS=2
      IF (FMIN-ACC) 7,7,83
C     CALCULATE THE CHANGES IN X AND IN F
  107 DS=0.0D0
      DO 114 I=1,N
      X(I)=X(I)-W(NWX+I)
      DS=DS+X(I)*X(I)
  114 CONTINUE
      DO 115 I=1,M
      F(I)=F(I)-W(NWF+I)
  115 CONTINUE
C     CALCULATE THE GENERALIZED INVERSE TIMES THE CHANGE IN X
      K=NWI
      SS=0.0D0
      DO 116 I=1,MPN
      SP=0.0D0
      DO 117 J=1,N
      K=K+1
      SP=SP+W(K)*X(J)
  117 CONTINUE
      W(NWV+I)=SP
      SS=SS+SP*SP
  116 CONTINUE
C     CALCULATE J TIMES THE CHANGE IN F
C     ALSO APPLY PROJECTION TO THE GENERALIZED INVERSE
      DO 118 I=1,N
      ST=0.0D0
      K=NWI+I
      DO 119 J=1,MPN
      ST=ST+W(K)*W(J+NWV)
      K=K+N
  119 CONTINUE
      ST=ST/SS
      K=NWI+I
      DO 120 J=1,MPN
      W(K)=W(K)-ST*W(J+NWV)
      K=K+N
  120 CONTINUE
      ST=PPAR*X(I)
      K=I
      DO 121 J=1,M
      ST=ST+W(K)*F(J)
      K=K+N
  121 CONTINUE
      W(NWW+I)=ST
  118 CONTINUE
C     REVISE J AND CALCULATE ROW VECTOR FOR CORRECTION TO INVERSE
      IC=0
      K=0
      KK=NWI
      SP=0.0D0
      SPP=0.0D0
      DO 122 I=1,M
      SS=F(I)
      ST=F(I)
      DO 123 J=1,N
      IC=IC+1
      KK=KK+1
      SS=SS-W(IC)*X(J)
      ST=ST-W(KK)*W(NWW+J)
  123 CONTINUE
      SS=SS/DS
      W(NWV+I)=ST
      SP=SP+F(I)*ST
      SPP=SPP+ST*ST
      DO 124 J=1,N
      K=K+1
      W(K)=W(K)+SS*X(J)
  124 CONTINUE
  122 CONTINUE
      DO 125 I=1,N
      ST=PAR*X(I)
      DO 126 J=1,N
      KK=KK+1
      ST=ST-W(KK)*W(NWW+J)
  126 CONTINUE
      W(NWT+I)=ST
      SP=SP+PAR*X(I)*ST
      SPP=SPP+ST*ST
  125 CONTINUE
C     TEST THAT THE SCALAR PRODUCT IS SUFFICIENTLY ACCURATE
      IF(0.01D0*SPP-DABS(SP-SPP))63,63,127
C     CALCULATE THE NEW GENERALIZED INVERSE
  127 DO 128 I=1,N
      K=NWI+I
      ST=X(I)
      DO 129 J=1,M
      ST=ST-W(K)*F(J)
      K=K+N
  129 CONTINUE
      SS=0.0D0
      DO 130 J=1,N
      SS=SS+W(K)*X(J)
      K=K+N
  130 CONTINUE
      ST=(ST-PAR*SS)/SP
      K=NWI+I
      DO 131 J=1,MPN
      W(K)=W(K)+ST*W(NWV+J)
      K=K+N
  131 CONTINUE
  128 CONTINUE
      GO TO 64
      END


C*********************************************************************
C*************** UNTERPROGRAMM  ma10ad.hsl  **************************

C HSL VERSION CONTROL RECORD
C ma10ad.f 11.1 93/05/06
C@PROCESS DIRECTIVE('IBMD')
C######DATE   01 JAN 1984     COPYRIGHT UKAEA, HARWELL.
C######ALIAS MA10AD
C###### CALLS
      SUBROUTINE MA10AD(A,B,M,N,IERR,IOPT,IA,IB)
      COMMON /MA10BD/LP,IPIVOT
      DOUBLE PRECISION A(IA,*),B(IB,*),W
      DOUBLE PRECISION SCPROD,ZERO
      EXTERNAL MA10CD
      DATA ZERO/0.0D0/
      IF(M.LE.0)GO TO 420
      IF(IOPT.LT.0)GO TO 320
C
C SET ELEMENTS OF THE UPPER TRIANGLE
      IF(M.EQ.1)GO TO 140
      DO 130 I=1,M-1
      DO 120 J=I+1,M
      A(I,J)=A(J,I)
  120 CONTINUE
  130 CONTINUE
C
C PERFORM CHOLESKII FACTORIZATION AND FORWARD ELIMINATION
140   DO 250 I=1,M
      SCPROD=ZERO
      DO 1155 IPROD=1,I-1
      SCPROD=SCPROD+A(IPROD,I)*A(IPROD,I)
 1155 CONTINUE
      A(I,I)=A(I,I)-SCPROD
      IF(A(I,I).LE.0.0)GO TO 440
      A(I,I)=DSQRT(A(I,I))
CIBMD IGNORE RECRDEPS
      DO 190 J=I+1,M
      SCPROD=ZERO
      DO 155 IPROD=1,I-1
      SCPROD=SCPROD+A(IPROD,I)*A(IPROD,J)
  155 CONTINUE
      A(I,J)=(A(I,J)-SCPROD)/A(I,I)
  190 CONTINUE
      IF(N.LE.0)GO TO 250
      IF(IOPT.LT.0)GO TO 250
CIBMD IGNORE RECRDEPS
      DO 240 J=1,N
      SCPROD=ZERO
      DO 215 IPROD=1,I-1
      SCPROD=SCPROD+A(IPROD,I)*B(IPROD,J)
  215 CONTINUE
      B(I,J)=(B(I,J)-SCPROD)/A(I,I)
  240 CONTINUE
  250 CONTINUE
      IF(N.LE.0)GO TO 310
C
C BACK-SUBSTITUTION
      DO 300 J=1,N
      B(M,J)=B(M,J)/A(M,M)
      IF(M.EQ.1)GO TO 300
      DO 290 I=M-1,1,-1
      SCPROD=ZERO
      DO 275 IPROD=I+1,M
      SCPROD=SCPROD+A(I,IPROD)*B(IPROD,J)
  275 CONTINUE
      B(I,J)=(B(I,J)-SCPROD)/A(I,I)
  290 CONTINUE
  300 CONTINUE
310   IF(IOPT.LE.0)GO TO 420
C
C CALCULATE INVERSE
  320 DO 330 I=1,M
      A(I,I)=1.0/A(I,I)
  330 CONTINUE
      DO 390 I2=1,M
      I=M+1-I2
      W=A(I,I)
      SCPROD=ZERO
      DO 347 IPROD=I+1,M
      SCPROD=SCPROD+A(IPROD,I)*A(I,IPROD)
  347 CONTINUE
      A(I,I)=A(I,I)*(W-SCPROD)
CIBMD IGNORE RECRDEPS
      DO 1380 J=I-1,1,-1
      SCPROD=ZERO
      DO 1377 IPROD=I+1,M
      SCPROD=SCPROD+A(IPROD,I)*A(J,IPROD)
 1377 CONTINUE
      A(I,J)=SCPROD
 1380 CONTINUE
C     DO 380 J=I-1,1,-1
C     SCPROD=A(I,J)
C     DO 375 IPROD=J+1,I
C     SCPROD=SCPROD+A(I,IPROD)*A(J,IPROD)
C 375 CONTINUE
C     A(I,J)=-SCPROD*A(J,J)
C 380 CONTINUE
      DO 380 J=I-1,1,-1
      A(I,J)=-(A(I,J)+A(I,I)*A(J,I))*A(J,J)
      DO 375 K=1,J-1
      A(I,K)=A(I,K)+A(I,J)*A(K,J)
  375 CONTINUE
  380 CONTINUE
  390 CONTINUE
C
C SET ELEMENTS OF THE UPPER TRIANGLE OF THE INVERSE
      IF(M.EQ.1)GO TO 420
      DO 410 I=1,M-1
      DO 400 J=I+1,M
      A(I,J)=A(J,I)
  400 CONTINUE
  410 CONTINUE
C
C SET RETURN FLAGS
420   IERR=0
      IPIVOT=0
      GO TO 460
  440 IF(LP.GT.0)WRITE(LP,450)
450   FORMAT(50H MA10 HAS BEEN GIVEN A MATRIX THAT IS NOT POSITIVE
     1,9H DEFINITE)
      IPIVOT=I
      IERR=1
460   RETURN
      END
      BLOCK DATA MA10CD
      COMMON /MA10BD/LP,IPIVOT
      DATA LP/20/
      END


C*********************************************************************
C**************  UNTERPROGRAMM  mb11ad.hsl  **************************

*######DATE 7 Dec 1992 COPYRIGHT Rutherford Appleton Laboratory
C       Toolpack tool decs employed.
C       Make ZERO and ONE PARAMETER.
C       Change DFLOAT to DBLE.
C       Change arg dimensions to *.
C       Remove MB11CD reference from MB11AD
C       SAVE statements added.
C
C  EAT 21/6/93 EXTERNAL statement put in for block data so will work on
C
C
      SUBROUTINE MB11AD(M,N,A,IA,W)
C     PARTITION THE WORKING SPACE ARRAY W
C     THE FIRST PARTITION HOLDS THE FIRST COMPONENTS OF THE VECTORS OF
C     THE ELEMENTARY TRANSFORMATIONS
C     .. Parameters ..
      DOUBLE PRECISION ONE,ZERO
      PARAMETER (ONE=1.0D0,ZERO=0.0D0)
C     ..
C     .. Scalar Arguments ..
      INTEGER IA,M,N
C     ..
C     .. Array Arguments ..
      DOUBLE PRECISION A(IA,*),W(*)
C     ..
C     .. Local Scalars ..
      DOUBLE PRECISION AKK,BSQ,RMAX,SIGMA,SUM,WKK
      INTEGER I,IR,J,KK,KP,MMK,N1,N2,N3,N4,N5,N6,NCW,NRW
C     ..
C     .. External Subroutines ..
      EXTERNAL MB11DD,MB11ED,MB11FD
C     ..
C     .. Data block external statement
      EXTERNAL MB11CD
C     ..
C     .. Intrinsic Functions ..
      INTRINSIC DABS,DBLE,DSIGN,DSQRT,IDINT
C     ..
C     .. Common blocks ..
      COMMON /MB11BD/LP
      INTEGER LP
C     ..
C     .. Save statement ..
      SAVE /MB11BD/
C     ..
C     .. Executable Statements ..
      NRW = M
C     THE SECOND PARTITION RECORDS ROW INTERCHANGES
      NCW = M + M
C     THE THIRD PARTITION RECORDS COLUMN INTERCHANGES
C     SET THE INITIAL RECORDS OF ROW AND COLUMN INTERCHANGES
      DO 1 I = 1,M
        N1 = NRW + I
        W(N1) = 0.5D0 + DBLE(I)
    1 CONTINUE
      DO 2 I = 1,N
        N1 = NCW + I
        W(N1) = 0.5D0 + DBLE(I)
    2 CONTINUE
C     'KK' COUNTS THE SEPARATE ELEMENTARY TRANSFORMATIONS
      KK = 1
C     FIND LARGEST ROW AND MAKE ROW INTERCHANGES
    3 RMAX = 0.0D0
      DO 4 I = KK,M
        SUM = 0.0D0
        DO 5 J = KK,N
          SUM = SUM + A(I,J)**2
    5   CONTINUE
        IF (RMAX-SUM) 6,4,4
    6   RMAX = SUM
        IR = I
    4 CONTINUE
      IF (RMAX.EQ.0.0D0) GO TO 81
      IF (IR-KK) 7,7,8
    8 N3 = NRW + KK
      SUM = W(N3)
      N4 = NRW + IR
      W(N3) = W(N4)
      W(N4) = SUM
      DO 9 J = 1,N
        SUM = A(KK,J)
        A(KK,J) = A(IR,J)
        A(IR,J) = SUM
    9 CONTINUE
C     FIND LARGEST ELEMENT OF PIVOTAL ROW, AND MAKE COLUMN INTERCHANGES
    7 RMAX = 0.0D0
      SUM = 0.0D0
      DO 10 J = KK,N
        SUM = SUM + A(KK,J)**2
        IF (RMAX-DABS(A(KK,J))) 11,10,10
   11   RMAX = DABS(A(KK,J))
        IR = J
   10 CONTINUE
      IF (IR-KK) 12,12,13
   13 N5 = NCW + KK
      RMAX = W(N5)
      N6 = NCW + IR
      W(N5) = W(N6)
      W(N6) = RMAX
      DO 14 I = 1,M
        RMAX = A(I,KK)
        A(I,KK) = A(I,IR)
        A(I,IR) = RMAX
   14 CONTINUE
C     REPLACE THE PIVOTAL ROW BY THE VECTOR OF THE TRANSFORMATION
   12 SIGMA = DSQRT(SUM)
      BSQ = DSQRT(SUM+SIGMA*DABS(A(KK,KK)))
      W(KK) = DSIGN(SIGMA+DABS(A(KK,KK)),A(KK,KK))/BSQ
      A(KK,KK) = -DSIGN(SIGMA,A(KK,KK))
      KP = KK + 1
      IF (KP-N) 15,15,16
   15 DO 17 J = KP,N
        A(KK,J) = A(KK,J)/BSQ
   17 CONTINUE
C     APPLY THE TRANSFORMATION TO THE REMAINING ROWS OF A
      IF (KP-M) 18,18,16
   18 WKK = W(KK)
      CALL MB11DD(A(KK+1,KK+1),A(KK+1,KK),A(KK,KK+1),IA,WKK,M-KK,N-KK)
      KK = KP
      GO TO 3
C     AT THIS STAGE THE REDUCTION OF A IS COMPLETE
C     NOW WE BUILD UP THE GENERALIZED INVERSE
C     APPLY THE FIRST ELEMENTARY TRANSFORMATION
   16 KK = M
      KP = M + 1
      SUM = -W(M)/A(M,M)
      IF (N-M) 33,33,34
   34 DO 35 J = KP,N
        A(M,J) = SUM*A(M,J)
   35 CONTINUE
   33 A(M,M) = 1.0D0/A(M,M) + SUM*W(M)
C     NOW APPLY THE OTHER (M-1) TRANSFORMATIONS
   36 KP = KK
      KK = KP - 1
      IF (KK) 37,37,38
C     FIRST TRANSFORM THE LAST (M-KK) ROWS
   38 WKK = W(KK)
      CALL MB11ED(A(KK+1,KK+1),A(KK,KK+1),IA,W(KK+1),WKK,M-KK,N-KK)
C     THEN CALCULATE THE NEW ROW IN POSITION KK
      AKK = ONE/A(KK,KK)
      CALL MB11FD(A(KK+1,KK+1),A(KK,KK+1),A(KK+1,KK),IA,WKK,AKK,M-KK,
     +            N-KK)
C     AND REVISE THE COLUMN IN POSITION KK
      SUM = 1.0D0 - WKK**2
      DO 44 I = KP,M
        SUM = SUM - A(I,KK)*W(I)
        A(I,KK) = W(I)
   44 CONTINUE
      A(KK,KK) = SUM/A(KK,KK)
      GO TO 36
C     RESTORE THE ROW INTERCHANGES
   37 DO 45 I = 1,M
   46   N1 = NRW + I
        IR = IDINT(W(N1))
        IF (I-IR) 47,45,45
   47   SUM = W(N1)
        N2 = NRW + IR
        W(N1) = W(N2)
        W(N2) = SUM
        DO 48 J = 1,N
          SUM = A(I,J)
          A(I,J) = A(IR,J)
          A(IR,J) = SUM
   48   CONTINUE
        GO TO 46

   45 CONTINUE
C     RESTORE THE COLUMN INTERCHANGES
      DO 49 J = 1,N
   50   N1 = NCW + J
        IR = IDINT(W(N1))
        IF (J-IR) 51,49,49
   51   SUM = W(N1)
        N2 = NCW + IR
        W(N1) = W(N2)
        W(N2) = SUM
        DO 52 I = 1,M
          SUM = A(I,J)
          A(I,J) = A(I,IR)
          A(I,IR) = SUM
   52   CONTINUE
        GO TO 50

   49 CONTINUE
   80 RETURN

   81 IF (LP.LE.0) GO TO 80
      MMK = M - KK
      WRITE (LP,FMT=82) MMK

   82 FORMAT (1H0,22H *** MB11AD ERROR *** ,I3,8H REDUCED,
     +       22H ROWS FOUND TO BE ZERO)

      STOP

      END
      BLOCK DATA MB11CD
C     .. Common blocks ..
      COMMON /MB11BD/LP
      INTEGER LP
C     ..
C     .. Save statement ..
      SAVE /MB11BD/
C     ..
C     .. Data statements ..
      DATA LP/6/
C     ..
C     .. Executable Statements ..
      END
C@PROCESS DIRECTIVE('IBMD')


      SUBROUTINE MB11DD(A,B,C,IA,WKK,MKK,NKK)
C     .. Scalar Arguments ..
      DOUBLE PRECISION WKK
      INTEGER IA,MKK,NKK
C     ..
C     .. Array Arguments ..
      DOUBLE PRECISION A(IA,NKK),B(*),C(IA,NKK)
C     ..
C     .. Local Scalars ..
      DOUBLE PRECISION SUM
      INTEGER I,J
C     ..
C     .. Executable Statements ..
      DO 19 I = 1,MKK
        SUM = WKK*B(I)
CIBMD PREFER SCALAR
        DO 20 J = 1,NKK
          SUM = SUM + C(1,J)*A(I,J)
   20   CONTINUE
        SUM = -SUM
        B(I) = B(I) + SUM*WKK
CIBMD PREFER SCALAR
        DO 21 J = 1,NKK
          A(I,J) = A(I,J) + SUM*C(1,J)
   21   CONTINUE
   19 CONTINUE
      RETURN

      END
C@PROCESS DIRECTIVE('IBMD')


      SUBROUTINE MB11ED(A,B,IA,W,WKK,MKK,NKK)
C     .. Scalar Arguments ..
      DOUBLE PRECISION WKK
      INTEGER IA,MKK,NKK
C     ..
C     .. Array Arguments ..
      DOUBLE PRECISION A(IA,NKK),B(IA,NKK),W(*)
C     ..
C     .. Local Scalars ..
      DOUBLE PRECISION SUM
      INTEGER I,J
C     ..
C     .. Executable Statements ..
      DO 39 I = 1,MKK
        SUM = 0.0D0
CIBMD PREFER SCALAR
        DO 40 J = 1,NKK
          SUM = SUM + B(1,J)*A(I,J)
   40   CONTINUE
        SUM = -SUM
CIBMD PREFER SCALAR
        DO 41 J = 1,NKK
          A(I,J) = A(I,J) + SUM*B(1,J)
   41   CONTINUE
        W(I) = SUM*WKK
   39 CONTINUE
      RETURN

      END
C@PROCESS DIRECTIVE('IBMD')


      SUBROUTINE MB11FD(A,B,C,IA,WKK,AKK,MKK,NKK)
C     .. Scalar Arguments ..
      DOUBLE PRECISION AKK,WKK
      INTEGER IA,MKK,NKK
C     ..
C     .. Array Arguments ..
      DOUBLE PRECISION A(IA,*),B(IA,*),C(*)
C     ..
C     .. Local Scalars ..
      DOUBLE PRECISION SUM
      INTEGER I,J
C     ..
C     .. Executable Statements ..
      DO 42 J = 1,NKK
        SUM = WKK*B(1,J)
        DO 43 I = 1,MKK
          SUM = SUM + C(I)*A(I,J)
   43   CONTINUE
        SUM = -SUM
        B(1,J) = SUM*AKK
   42 CONTINUE
      RETURN

      END




* *******************************************************************
* COPYRIGHT (c) 1972 Hyprotech UK
* All rights reserved.
*
* None of the comments in this Copyright notice between the lines
* of asterisks shall be removed or altered in any way.
*
* This Package is intended for compilation without modification,
* so most of the embedded comments have been removed.
*
* ALL USE IS SUBJECT TO LICENCE. For full details of an HSL ARCHIVE
* Licence, see http://hsl.rl.ac.uk/archive/cou.html
*
* Please note that for an HSL ARCHIVE Licence:
*
* 1. The Package must not be copied for use by any other person.
*    Supply of any part of the library by the Licensee to a third party
*    shall be subject to prior written agreement between AEA
*    Hyprotech UK Limited and the Licensee on suitable terms and
*    conditions, which will include financial conditions.
* 2. All information on the Package is provided to the Licensee on the
*    understanding that the details thereof are confidential.
* 3. All publications issued by the Licensee that include results obtained
*    with the help of one or more of the Packages shall acknowledge the
*    use of the Packages. The Licensee will notify the Numerical Analysis
*    Group at Rutherford Appleton Laboratory of any such publication.
* 4. The Packages may be modified by or on behalf of the Licensee
*    for such use in research applications but at no time shall such
*    Packages or modifications thereof become the property of the
*    Licensee. The Licensee shall make available free of charge to the
*    copyright holder for any purpose all information relating to
*    any modification.
* 5. Neither CCLRC nor Hyprotech UK Limited shall be liable for any
*    direct or consequential loss or damage whatsoever arising out of
*    the use of Packages by the Licensee.
* *******************************************************************
*
*######DATE 22 Dec 1992
C       Toolpack tool decs employed.
C       ABSI reference removed.
C       AIT(1) initialized to zero.
C       Label 17 removed.
C       DFLOAT -> DBLE.
C       SAVE statements added.
C       QA05CD reference removed.
C
C  EAT 21/6/93 EXTERNAL statement put in for block data so will work on VAXs.
C  EAT 8/7/93 EXTERNAL statement removed.
C
      SUBROUTINE QA05AD(VAL,FUNC,A,B,AERR,RERR,LEVEL,ERROR,IFLAG)
      DOUBLE PRECISION A,AERR,B,ERROR,RERR,VAL
      INTEGER IFLAG,LEVEL
      DOUBLE PRECISION FUNC
      EXTERNAL FUNC
      DOUBLE PRECISION AITLOW,AITTOL,ALG402,ALPHA,ASTEP,BEG,CUREST,DIFF,
     +                 END,ERGOAL,ERRA,ERRER,ERRR,FBEG,FBEG2,FEND,
     +                 FEXTM1,FEXTRP,FN,FNSIZE,H2NEXT,H2TFEX,H2TOL,HOVN,
     +                 JUMPTL,LENGTH,PREVER,SING,SINGNX,SLOPE,STAGE,
     +                 STEP,STEPMN,SUM,SUMABS,TABS,TABTLM,TOLMCH,TOLSAV,
     +                 VINT
      INTEGER I,IBEG,IEND,II,III,ISTAGE,ISTEP,ISTEP2,IT,J,L,LM1,MAXTBL,
     +        MAXTS,MXSTGE,N,N2,NNLEFT
      LOGICAL AITKEN,H2CONV,REGLAR,RIGHT
      DOUBLE PRECISION AIT(10),BEGIN(30),DIF(10),EST(30),FINIS(30),
     +                 R(10),RN(4),T(10,10),TS(2049)
      INTEGER IBEGS(30)
      LOGICAL REGLSV(30)
      DOUBLE PRECISION FD05AD
      EXTERNAL FD05AD
      INTRINSIC DABS,DBLE,DLOG10,DMAX1,DMIN1,FLOAT,MAX0
      COMMON /QA05DD/IPR
      INTEGER IPR
      SAVE /QA05DD/,AITLOW,H2TOL,AITTOL,JUMPTL,MAXTS,MAXTBL,MXSTGE,RN,
     +     ALG402
      DATA AITLOW,H2TOL,AITTOL,JUMPTL,MAXTS,MAXTBL,MXSTGE/1.1D0,.15D0,
     +     .1D0,.01D0,2049,10,30/
      DATA RN(1)/.71420053D0/,RN(2)/.34662815D0/,RN(3)/.843751D0/,
     +     RN(4)/.12633046D0/
      DATA ALG402/.3010299956639795D0/
      TOLSAV = FD05AD(1)*10.0D0
      TOLMCH = TOLSAV
      VAL = 0.D0
      ERROR = 0.D0
      IFLAG = 1
      LENGTH = DABS(B-A)
      IF (LENGTH.EQ.0.D0) RETURN
      ERRR = DMIN1(.1D0,DMAX1(DABS(RERR),10.D0*TOLMCH))
      ERRA = DABS(AERR)
      STEPMN = DMAX1(LENGTH/DBLE(2**MXSTGE),
     +         DMAX1(LENGTH,DABS(A),DABS(B))*TOLMCH)
      STAGE = .5D0
      ISTAGE = 1
      CUREST = 0.D0
      FNSIZE = 0.D0
      PREVER = 0.D0
      REGLAR = .FALSE.
      BEG = A
      FBEG = FUNC(A)/2.D0
      TS(1) = FBEG
      IBEG = 1
      END = B
      FEND = FUNC(B)/2.D0
      TS(2) = FEND
      IEND = 2
      AIT(1) = 0.0D0
    5 RIGHT = .FALSE.
    6 STEP = END - BEG
      ASTEP = DABS(STEP)
      IF (ASTEP.LT.STEPMN) GO TO 950
      IF (LEVEL.GE.3) WRITE (IPR,FMT=609) BEG,STEP,ISTAGE
  609 FORMAT (10H BEG,STEP ,2E16.8,I5)
      T(1,1) = FBEG + FEND
      TABS = DABS(FBEG) + DABS(FEND)
      L = 1
      N = 1
      H2CONV = .FALSE.
      AITKEN = .FALSE.
      GO TO 10
    9 IF (LEVEL.GE.4) WRITE (IPR,FMT=692) L,T(1,LM1)
   10 LM1 = L
      L = L + 1
      N2 = N*2
      FN = N2
      ISTEP = (IEND-IBEG)/N
      IF (ISTEP.GT.1) GO TO 12
      II = IEND
      IEND = IEND + N
      IF (IEND.GT.MAXTS) GO TO 900
      HOVN = STEP/FN
      III = IEND
      DO 11 I = 1,N2,2
        TS(III) = TS(II)
        TS(III-1) = FUNC(END-FLOAT(I)*HOVN)
        III = III - 2
   11 II = II - 1
      ISTEP = 2
   12 ISTEP2 = IBEG + ISTEP/2
      SUM = 0.D0
      SUMABS = 0.D0
      DO 13 I = ISTEP2,IEND,ISTEP
        SUM = SUM + TS(I)
   13 SUMABS = SUMABS + DABS(TS(I))
      T(L,1) = T(L-1,1)/2.D0 + SUM/FN
      TABS = TABS/2.D0 + SUMABS/FN
      N = N2
      IT = 1
      VINT = STEP*T(L,1)
      TABTLM = TABS*TOLMCH
      FNSIZE = DMAX1(FNSIZE,DABS(T(L,1)))
      ERGOAL = DMAX1(ASTEP*TOLMCH*FNSIZE,
     +         STAGE*DMAX1(ERRA,ERRR*DABS((CUREST)+VINT)))
      FEXTRP = 1.D0
      DO 14 I = 1,LM1
        FEXTRP = FEXTRP*4.D0
        T(I,L) = T(L,I) - T(L-1,I)
   14 T(L,I+1) = T(L,I) + T(I,L)/ (FEXTRP-1.D0)
      ERRER = ASTEP*DABS(T(1,L))
      IF (L.GT.2) GO TO 15
      IF (DABS(T(1,2)).LE.TABTLM) GO TO 60
      GO TO 10
   15 DO 16 I = 2,LM1
        DIFF = 0.D0
        IF (DABS(T(I-1,L)).GT.TABTLM) DIFF = T(I-1,LM1)/T(I-1,L)
   16 T(I-1,LM1) = DIFF
      IF (DABS(4.D0-T(1,LM1)).LE.H2TOL) GO TO 20
      IF (T(1,LM1).EQ.0.D0) GO TO 18
      IF (DABS(2.D0-DABS(T(1,LM1))).LT.JUMPTL) GO TO 50
      IF (L.EQ.3) GO TO 9
      H2CONV = .FALSE.
      IF (DABS((T(1,LM1)-T(1,L-2))/T(1,LM1)).LE.AITTOL) GO TO 30
      IF (REGLAR) GO TO 18
      IF (L.EQ.4) GO TO 9
   18 IF (ERRER.LE.ERGOAL) GO TO 70
      IF (LEVEL.GE.4) WRITE (IPR,FMT=692) L,T(1,LM1)
      GO TO 91
   20 IF (LEVEL.GE.4) WRITE (IPR,FMT=619) L,T(1,LM1)
  619 FORMAT (I5,E16.8,5X,6HH2CONV)
      IF (H2CONV) GO TO 21
      AITKEN = .FALSE.
      H2CONV = .TRUE.
      IF (LEVEL.GE.3) WRITE (IPR,FMT=620) L
  620 FORMAT (22H H2 CONVERGENCE AT ROW,I3)
   21 FEXTRP = 4.D0
   22 IT = IT + 1
      VINT = STEP*T(L,IT)
      ERRER = DABS(STEP/ (FEXTRP-1.D0)*T(IT-1,L))
      IF (ERRER.LE.ERGOAL) GO TO 80
      IF (IT.EQ.LM1) GO TO 40
      IF (T(IT,LM1).EQ.0.D0) GO TO 22
      IF (T(IT,LM1).LE.FEXTRP) GO TO 40
      IF (DABS(T(IT,LM1)/4.D0-FEXTRP)/FEXTRP.LT.AITTOL) FEXTRP = FEXTRP*
     +    4.D0
      GO TO 22
   30 IF (LEVEL.GE.4) WRITE (IPR,FMT=629) L,T(1,LM1)
  629 FORMAT (I5,E16.8,5X,6HAITKEN)
      IF (T(1,LM1).LT.AITLOW) GO TO 91
      IF (AITKEN) GO TO 31
      H2CONV = .FALSE.
      AITKEN = .TRUE.
      IF (LEVEL.GE.3) WRITE (IPR,FMT=630) L
  630 FORMAT (14H AITKEN AT ROW,I3)
   31 FEXTRP = T(L-2,LM1)
      IF (FEXTRP.GT.4.5) GO TO 21
      IF (FEXTRP.LT.AITLOW) GO TO 91
      IF (DABS(FEXTRP-T(L-3,LM1))/T(1,LM1).GT.H2TOL) GO TO 91
      IF (LEVEL.GE.3) WRITE (IPR,FMT=631) FEXTRP
  631 FORMAT (6H RATIO,F12.8)
      SING = FEXTRP
      FEXTM1 = FEXTRP - 1.0D0
      DO 32 I = 2,L
        AIT(I) = T(I,1) + (T(I,1)-T(I-1,1))/FEXTM1
        R(I) = T(1,I-1)
   32 DIF(I) = AIT(I) - AIT(I-1)
      IT = 2
   33 VINT = STEP*AIT(L)
      IF (LEVEL.LT.5) GO TO 333
      WRITE (IPR,FMT=632) (R(I+1),I=IT,LM1)
      WRITE (IPR,FMT=632) (AIT(I),I=IT,L)
      WRITE (IPR,FMT=632) (DIF(I+1),I=IT,LM1)
  632 FORMAT (1X,8E15.8)
  333 ERRER = ERRER/FEXTM1
      IF (ERRER.GT.ERGOAL) GO TO 34
      ALPHA = DLOG10(SING)/ALG402 - 1.D0
      IF (LEVEL.GE.2) WRITE (IPR,FMT=633) ALPHA,BEG,END
  633 FORMAT (11X,42HINTEGRAND SHOWS SINGULAR BEHAVIOUR OF TYPE,5H X**(,
     +       F4.2,9H) BETWEEN,E15.8,4H AND,E15.8)
      IFLAG = MAX0(IFLAG,2)
      GO TO 80
   34 IT = IT + 1
      IF (IT.EQ.LM1) GO TO 40
      IF (IT.GT.3) GO TO 35
      H2NEXT = 4.D0
      SINGNX = 2.D0*SING
   35 IF (H2NEXT.LT.SINGNX) GO TO 36
      FEXTRP = SINGNX
      SINGNX = 2.D0*SINGNX
      GO TO 37
   36 FEXTRP = H2NEXT
      H2NEXT = 4.D0*H2NEXT
   37 DO 38 I = IT,LM1
        R(I+1) = 0.D0
   38 IF (DABS(DIF(I+1)).GT.TABTLM) R(I+1) = DIF(I)/DIF(I+1)
      IF (LEVEL.GE.4) WRITE (IPR,FMT=638) FEXTRP,R(L-1),R(L)
  638 FORMAT (16H FEXTRP + RATIOS,3E15.8)
      H2TFEX = -H2TOL*FEXTRP
      IF (R(L)-FEXTRP.LT.H2TFEX) GO TO 40
      IF (R(L-1)-FEXTRP.LT.H2TFEX) GO TO 40
      ERRER = ASTEP*DABS(DIF(L))
      FEXTM1 = FEXTRP - 1.D0
      DO 39 I = IT,L
        AIT(I) = AIT(I) + DIF(I)/FEXTM1
   39 DIF(I) = AIT(I) - AIT(I-1)
      GO TO 33
   40 FEXTRP = DMAX1(PREVER/ERRER,AITLOW)
      PREVER = ERRER
      IF (L.LT.5) GO TO 10
      IF (LEVEL.GE.3) WRITE (IPR,FMT=641) ERRER,ERGOAL,FEXTRP,IT
  641 FORMAT (23H ERRER,ERGOAL,FEXTRP,IT,2E15.8,E14.5,I3)
      IF (L-IT.GT.2 .AND. ISTAGE.LT.MXSTGE) GO TO 90
      IF (ERRER/FEXTRP** (MAXTBL-L).LT.ERGOAL) GO TO 10
      GO TO 90
   50 IF (LEVEL.GE.4) WRITE (IPR,FMT=649) L,T(1,LM1)
  649 FORMAT (I5,E16.8,5X,4HJUMP)
      IF (ERRER.GT.ERGOAL) GO TO 90
      DIFF = DABS(T(1,L))*2.D0*FN
      IF (LEVEL.GE.2) WRITE (IPR,FMT=650) DIFF,BEG,END
  650 FORMAT (13X,36HINTEGRAND SEEMS TO HAVE JUMP OF SIZE,E13.6,
     +       8H BETWEEN,E15.8,4H AND,E15.8)
      GO TO 80
   60 IF (LEVEL.GE.4) WRITE (IPR,FMT=660) L
  660 FORMAT (I5,21X,13HSTRAIGHT LINE)
      SLOPE = (FEND-FBEG)*2.D0
      FBEG2 = FBEG*2.D0
      DO 61 I = 1,4
        DIFF = DABS(FUNC(BEG+RN(I)*STEP)-FBEG2-RN(I)*SLOPE)
        IF (DIFF.GT.TABTLM) GO TO 72
   61 CONTINUE
      IF (LEVEL.GE.3) WRITE (IPR,FMT=667) BEG,END
  667 FORMAT (27X,43HINTEGRAND SEEMS TO BE STRAIGHT LINE BETWEEN,E15.8,
     +       4H AND,E15.8)
      GO TO 80
   70 IF (LEVEL.GE.4) WRITE (IPR,FMT=670) L,T(1,LM1)
  670 FORMAT (I5,E16.8,5X,5HNOISE)
      SLOPE = (FEND-FBEG)*2.D0
      FBEG2 = FBEG*2.D0
      I = 1
   71 DIFF = DABS(FUNC(BEG+RN(I)*STEP)-FBEG2-RN(I)*SLOPE)
   72 ERRER = DMAX1(ERRER,ASTEP*DIFF)
      IF (ERRER.GT.ERGOAL) GO TO 91
      I = I + 1
      IF (I.LE.4) GO TO 71
      IF (LEVEL.GE.3) WRITE (IPR,FMT=671) BEG,END
  671 FORMAT (15H NOISE BETWEEN ,E15.8,4H AND,E15.8)
      IFLAG = 3
   80 VAL = VAL + VINT
      ERROR = ERROR + ERRER
      IF (LEVEL.LT.3) GO TO 83
      IF (LEVEL.LT.5) GO TO 82
      DO 81 I = 1,L
   81 WRITE (IPR,FMT=692) I, (T(I,J),J=1,L)
   82 WRITE (IPR,FMT=682) VINT,ERRER,L,IT
  682 FORMAT (12H INTEGRAL IS,E16.8,7H, ERROR,E15.8,9H  FROM T(,I1,1H,,
     +       I1,1H))
   83 IF (RIGHT) GO TO 85
      ISTAGE = ISTAGE - 1
      IF (ISTAGE.EQ.0) RETURN
      REGLAR = REGLSV(ISTAGE)
      BEG = BEGIN(ISTAGE)
      END = FINIS(ISTAGE)
      CUREST = CUREST - EST(ISTAGE+1) + VINT
      IEND = IBEG - 1
      FEND = TS(IEND)
      IBEG = IBEGS(ISTAGE)
      GO TO 94
   85 CUREST = CUREST + VINT
      STAGE = STAGE*2.D0
      IEND = IBEG
      IBEG = IBEGS(ISTAGE)
      END = BEG
      BEG = BEGIN(ISTAGE)
      FEND = FBEG
      FBEG = TS(IBEG)
      GO TO 5
   90 REGLAR = .TRUE.
   91 IF (ISTAGE.EQ.MXSTGE) GO TO 950
      IF (LEVEL.LT.5) GO TO 93
      DO 92 I = 1,L
   92 WRITE (IPR,FMT=692) I, (T(I,J),J=1,L)
  692 FORMAT (I5,7E16.8,/,3E16.8)
   93 IF (RIGHT) GO TO 95
      REGLSV(ISTAGE+1) = REGLAR
      BEGIN(ISTAGE) = BEG
      IBEGS(ISTAGE) = IBEG
      STAGE = STAGE/2.D0
   94 RIGHT = .TRUE.
      BEG = (BEG+END)/2.D0
      IBEG = (IBEG+IEND)/2
      TS(IBEG) = TS(IBEG)/2.D0
      FBEG = TS(IBEG)
      GO TO 6
   95 NNLEFT = IBEG - IBEGS(ISTAGE)
      IF (IEND+NNLEFT.GE.MAXTS) GO TO 900
      III = IBEGS(ISTAGE)
      II = IEND
      DO 96 I = III,IBEG
        II = II + 1
   96 TS(II) = TS(I)
      DO 97 I = IBEG,II
        TS(III) = TS(I)
   97 III = III + 1
      IEND = IEND + 1
      IBEG = IEND - NNLEFT
      FEND = FBEG
      FBEG = TS(IBEG)
      FINIS(ISTAGE) = END
      END = BEG
      BEG = BEGIN(ISTAGE)
      BEGIN(ISTAGE) = END
      REGLSV(ISTAGE) = REGLAR
      ISTAGE = ISTAGE + 1
      REGLAR = REGLSV(ISTAGE)
      EST(ISTAGE) = VINT
      CUREST = CUREST + EST(ISTAGE)
      GO TO 5
  900 IF (LEVEL.GE.2) WRITE (IPR,FMT=6900) BEG,END
 6900 FORMAT (37H TOO MANY FUNCTION EVALUATIONS AROUND,/,10X,E15.8,
     +       4H AND,E15.8)
      IFLAG = 4
      GO TO 999
  950 IFLAG = 5
      IF (LEVEL.LT.2) GO TO 999
      IF (LEVEL.LT.5) GO TO 959
      DO 958 I = 1,L
  958 WRITE (IPR,FMT=692) I, (T(I,J),J=1,L)
  959 WRITE (IPR,FMT=6959) BEG,END
 6959 FORMAT (12X,38HINTEGRAND SHOWS SINGULAR BEHAVIOUR OF ,
     +       20HUNKNOWN TYPE BETWEEN,E15.8,4H AND,E15.8)
  999 VAL = CUREST + VINT
      RETURN
      END
      BLOCK DATA QA05CD
      COMMON /QA05DD/IPR
      INTEGER IPR
      SAVE /QA05DD/
      DATA IPR/6/
      END


* COPYRIGHT (c) 1988 AEA Technology
*######DATE 21 Jan 1993
C       Toolpack tool decs employed.
C       SAVE statement added.
C 1/10/98 DC(3) not initialized to avoid SUN f90 failure
C 16 October 2001: STOP and WRITE statements removed.

      DOUBLE PRECISION FUNCTION FD05AD(INUM)
C----------------------------------------------------------------
C  Real constants for: IEEE double precision (8-byte arithmetic)
C
C  Obtained from H.S.L. subroutine ZE02AM.
C  Nick Gould and Sid Marlow, Harwell Laboratory, April 1988.
C----------------------------------------------------------------
C     .. Scalar Arguments ..
      INTEGER INUM
C     ..
C     .. Local Arrays ..
      DOUBLE PRECISION DC(5)
C     ..
C     .. Save statement ..
      SAVE DC
C     ..
C     .. Data statements ..
C
C  DC(1) THE SMALLEST POSITIVE NUMBER: 1.0 + DC(1) > 1.0.
C  DC(2) THE SMALLEST POSITIVE NUMBER: 1.0 - DC(2) < 1.0.
C  DC(3) THE SMALLEST NONZERO +VE REAL NUMBER.
C  DC(4) THE SMALLEST FULL PRECISION +VE REAL NUMBER.
C  DC(5) THE LARGEST FINITE +VE REAL NUMBER.
C
      DATA DC(1)/2.2204460492504D-16/
      DATA DC(2)/1.1102230246253D-16/
C     DATA DC(3)/4.9406564584126D-324/
      DATA DC(4)/2.2250738585073D-308/
      DATA DC(5)/1.7976931348622D+308/
C     ..
C     .. Executable Statements ..

      IF ( INUM .LE. 0 ) THEN
         FD05AD = DC( 1 )
      ELSE IF ( INUM .GE. 6 ) THEN
         FD05AD = DC( 5 )
      ELSE IF ( INUM .EQ. 3 ) THEN
         FD05AD = DC(4)/2.0D0**52
      ELSE
         FD05AD = DC( INUM )
      ENDIF
      RETURN
      END



* *******************************************************************
* COPYRIGHT (c) 1964 Hyprotech UK
* All rights reserved.
*
* None of the comments in this Copyright notice between the lines
* of asterisks shall be removed or altered in any way.
*
* This Package is intended for compilation without modification,
* so most of the embedded comments have been removed.
*
* ALL USE IS SUBJECT TO LICENCE. For full details of an HSL ARCHIVE
* Licence, see http://hsl.rl.ac.uk/archive/cou.html
*
* Please note that for an HSL ARCHIVE Licence:
*
* 1. The Package must not be copied for use by any other person.
*    Supply of any part of the library by the Licensee to a third party
*    shall be subject to prior written agreement between AEA
*    Hyprotech UK Limited and the Licensee on suitable terms and
*    conditions, which will include financial conditions.
* 2. All information on the Package is provided to the Licensee on the
*    understanding that the details thereof are confidential.
* 3. All publications issued by the Licensee that include results obtained
*    with the help of one or more of the Packages shall acknowledge the
*    use of the Packages. The Licensee will notify the Numerical Analysis
*    Group at Rutherford Appleton Laboratory of any such publication.
* 4. The Packages may be modified by or on behalf of the Licensee
*    for such use in research applications but at no time shall such
*    Packages or modifications thereof become the property of the
*    Licensee. The Licensee shall make available free of charge to the
*    copyright holder for any purpose all information relating to
*    any modification.
* 5. Neither CCLRC nor Hyprotech UK Limited shall be liable for any
*    direct or consequential loss or damage whatsoever arising out of
*    the use of Packages by the Licensee.
* *******************************************************************
*
*######DATE 17 February 1994
C       Toolpack tool decs employed.
C       SAVE statements added.
C
      SUBROUTINE VD01AD(ITEST,X,F,MAXFUN,ABSACC,RELACC,XSTEP)
      DOUBLE PRECISION ABSACC,F,RELACC,X,XSTEP
      INTEGER ITEST,MAXFUN
      DOUBLE PRECISION D,DA,DB,DC,FA,FB,FC,XINC
      INTEGER IINC,IS,MC
      INTRINSIC DABS
      SAVE
      GO TO (1,2,2) ITEST
    2 IS = 6 - ITEST
      ITEST = 1
      IINC = 1
      XINC = XSTEP + XSTEP
      MC = IS - 3
      IF (MC) 4,4,15
    3 MC = MC + 1
      IF (MAXFUN-MC) 12,15,15
   12 ITEST = 4
   43 X = DB
      F = FB
      IF (FB-FC) 15,15,44
   44 X = DC
      F = FC
   15 RETURN
    1 GO TO (5,6,7,8) IS
    8 IS = 3
    4 DC = X
      FC = F
      X = X + XSTEP
      GO TO 3
    7 IF (FC-F) 9,10,11
   10 X = X + XINC
      XINC = XINC + XINC
      GO TO 3
    9 DB = X
      FB = F
      XINC = -XINC
      GO TO 13
   11 DB = DC
      FB = FC
      DC = X
      FC = F
   13 X = DC + DC - DB
      IS = 2
      GO TO 3
    6 DA = DB
      DB = DC
      FA = FB
      FB = FC
   32 DC = X
      FC = F
      GO TO 14
    5 IF (FB-FC) 16,17,17
   17 IF (F-FB) 18,32,32
   18 FA = FB
      DA = DB
   19 FB = F
      DB = X
      GO TO 14
   16 IF (FA-FC) 21,21,20
   20 XINC = FA
      FA = FC
      FC = XINC
      XINC = DA
      DA = DC
      DC = XINC
   21 XINC = DC
      IF ((D-DB)* (D-DC)) 32,22,22
   22 IF (F-FA) 23,24,24
   23 FC = FB
      DC = DB
      GO TO 19
   24 FA = F
      DA = X
   14 IF (FB-FC) 25,25,29
   25 IINC = 2
      XINC = DC
      IF (FB-FC) 29,45,29
   29 D = (FA-FB)/ (DA-DB) - (FA-FC)/ (DA-DC)
      IF (D* (DB-DC)) 33,33,37
   37 D = 0.5D0* (DB+DC- (FB-FC)/D)
      IF (DABS(D-X)-DABS(ABSACC)) 34,34,35
   35 IF (DABS(D-X)-DABS(D*RELACC)) 34,34,36
   34 ITEST = 2
      GO TO 43
   36 IS = 1
      X = D
      IF ((DA-DC)* (DC-D)) 3,26,38
   38 IS = 2
      GO TO (39,40) IINC
   39 IF (DABS(XINC)-DABS(DC-D)) 41,3,3
   33 IS = 2
      GO TO (41,42) IINC
   41 X = DC
      GO TO 10
   40 IF (DABS(XINC-X)-DABS(X-DC)) 42,42,3
   42 X = 0.5D0* (XINC+DC)
      IF ((XINC-X)* (X-DC)) 26,26,3
   45 X = 0.5D0* (DB+DC)
      IF ((DB-X)* (X-DC)) 26,26,3
   26 ITEST = 3
      GO TO 43
      END


      SUBROUTINE KOVA(X,Y,W,PAR,dpar,H,M,N,IW,nr)
C
C
C     DIESE SUBROUTINE BERECHNET DIE STANDARDABWEICHUNG DER MIT
C     S.R. VA05AD GEFITTETEN PARAMETER UND DIE KORRELATIONSMATRIX
C
C     DIESE S.R. KANN NUR IM ZUSAMMENHANG MIT S.R. VA05AD BENUTZT
C     WERDEN DURCH FOLGENDE AUFRUFE:
C
C     CALL VA05AD(M,N,F,PAR,H,DMAX,ACC,MAXFUN,IPRINT,WO)
C
C     CALL KOVA(X,Y,W,PAR,HP,M,N,IW)
C
C     ZUSAETZLICH MUESSEN FOLGENDE COMMON-BLOECKE VEREINBART WERDEN:
C     1) IM RUFENDEN PROGRAMM
C        COMMON /CAL/ X(150),Y(150)
C        COMMON /GEW/ DSW(150)
C        COMMON /PG/ PGUES(10)
C
C     2) IN S.R. CALFUN
C        COMMON /FF/ ALOR(150)
C        COMMON /CAL/ X(150),Y(150)
C        COMMON /GEW/ DSW(150)
C
C     AUSSERDEM MUESSEN IN S.R. CALFUN DIE F(I) MIT DSW(I) MULTIPLIZIER
C     WERDEN, WOBEI DSW(I)=DSQRT(W(I)) IST.
C     DSW MUSS VOR DEM AUFRUF VON S.R. VA05AD DEFINIERT SEIN
C
C     PARAMETERBESCHREIBUNG
C     (WEITERE PARAMETERBESCHR. SIEHE VA05AD)
C
C     X(I),I=1,M        ABSZISSE DER MESSWERTE
C     Y(I),I=1,M        ORDINATE DER MESSWERTE
C     W(I),I=1,M        GEWICHTE
C     PAR(I),I=1,N      PARAMETER (VOR AUFRUF VON VA05AD: STARTWERTE)
C     PGUES(I),I=1,N    STARTWERTE DER PARAMETER
C     M                 ANZAHL DER MESSWERTE
C     N                 ANZAHL DER PARAMETER
C     IW                = 0 GEWICHTE WERDEN IN S.R. KOVA BERECHNET
C                           Y(I) ~= 0. --> W(I) = 1./Y(I)
C                           Y(I)  = 0. --> W(I) = 1.
C                       = 1 GEWICHTE MUESSEN AN S.R. KOVA UEBERGEBEN WE
C     HP                DELTA(PAR(L)) (ZUR BERECHNUNG DER NUMERISCHEN A
C                       Z. B. HP = 10.**(-6)
C     DSW(I),I=1,M      DSQRT(W(I))
C     ALOR(I),I=1,M     FUNKTIONSWERTE DER THEORIEFKT IN S.R. CALFUN
C
C     ALLE REAL-VARIABLEN MUESSEN MIT REAL*8 DEKLARIERT SEIN
C     DER TYP DER VARIABLEN (INTEGER ODER REAL) ENTSPRICHT
C     DER STANDARD-FORTRAN DEKLARARTION
C
C     ES WERDEN NOCH FOLGENDE UNTERPROGRAMME AUS DER HSL BENOETIGT:
C     1) MA10AD
C     2) MC03AD
C
C     EINSCHRAENKUNGEN:
C     M <= 250 UND N <= 9
C     ERFUELLEN M UND N NICHT DIESE BEDINGUNGEN,
C     MUESSEN DIE DIMENSIONSANGABEN IN DEN PROGRAMMEN
C     DIE ERSTE ZUWEISUNG IN S.R. KOVA ( IA = 9 )
C     UND DIE ERSTE ABFRAGE IN S.R. KOVA ENTSPRECHEND
C     GEAENDERT WERDEN
C
C
C
C     .. Scalar Arguments ..
      REAL*8 H
      INTEGER IW,M,N
C     ..
C     .. Array Arguments ..
      REAL*8 PAR(N),W(M),X(M),Y(M),dpar(n)
C     ..
C     .. Arrays in Common ..
      REAL*8 FCT(250),PAR1(15),PGUES(15)
C     ..
C     .. Local Scalars ..
      REAL*8 DIF,DP,FI2,SUM,SUMF,SUMG
      INTEGER I,IA,IB,J,K,L,NR
C     ..
C     .. Local Arrays ..
      REAL*8 AM(9,9),B(1,1),DF(250,9),F(250),FPL(250),PPL(250),RHO(9,9)
C     ..
C     .. External Functions ..
      REAL*8 DFLOAT
C     EXTERNAL DFLOAT
C     ..
C     .. External Subroutines ..
      EXTERNAL CALFUN,MA10AD
C     ..
C     .. Intrinsic Functions ..
      INTRINSIC SQRT
C     ..
C     .. Common blocks ..
      COMMON /FF/FCT
      COMMON /PG/PGUES,PAR1
C     ..

      IF (M.GT.250 .OR. N.GT.9 .OR. M.LT.0 .OR. N.LT.0) THEN
         WRITE (20,FMT=9090) M,N

      ELSE
         IA = 9
         IB = 1
C
         IF (IW.EQ.0) THEN
            DO 10 I = 1,M
               W(I) = 1.D0
               IF (Y(I).NE.0.D0) W(I) = 1.D0/Y(I)
   10       CONTINUE
         END IF

	 CALL CALFUN(M,N,F,PAR)
         SUMF = 0.D0
         SUMG = 0.D0
         DO 20 I = 1,M
            FI2 = F(I)*F(I)
            SUMF = SUMF + FI2
            SUMG = SUMG + FI2/W(I)
   20    CONTINUE
         SUMF = SUMF/DFLOAT(M-N)
         DO 30 I = 1,M
            FPL(I) = FCT(I)
   30    CONTINUE
         DO 40 I = 1,N
            PPL(I) = PAR(I)
   40    CONTINUE
         DO 50 L = 1,N
            DP = H*PPL(L)
            IF (DP.EQ.0.D0) DP = H
            PAR(L) = PPL(L) + DP
            CALL CALFUN(M,N,F,PAR)
            PAR(L) = PPL(L)
            DO 60 I = 1,M
               DF(I,L) = (FCT(I)-FPL(I))/DP
   60       CONTINUE
   50    CONTINUE

         write(20,*) 'Derivative Matrix:'
         write(20,'(2147483647i10)') (k,k=1,n)
         do i=1,m
           write(20,'(i3,400e10.2)') i,(df(i,k),k=1,n)
         end do

         DO 70 K = 1,N
            DO 80 J = 1,K
               SUM = 0.D0
               DO 90 I = 1,M
                  SUM = SUM + W(I)*DF(I,K)*DF(I,J)
   90          CONTINUE
               AM(K,J) = SUM
               AM(J,K) = SUM
   80       CONTINUE
   70    CONTINUE

         write(20,*)
         write(20,*) 'Curvature Matrix:'
         write(20,'(3x,2147483647i10)') (k,k=1,n)
         do i=1,n
           write(20,'(i3,400e10.2)') i,(am(i,k),k=1,n)
         end do

C
C     INVERSION DER MATRIX AM
C
         CALL MA10AD(AM,B,N,0,NR,1,IA,IB)
C
C
         IF (NR.EQ.0) THEN
            DO 100 I = 1,N
               DO 110 K = 1,I
                  RHO(I,K) = AM(I,K)/ (SQRT(AM(I,I))*SQRT(AM(K,K)))
                  RHO(K,I) = RHO(I,K)
  110          CONTINUE
  100       CONTINUE
C
C
C     OUTPUT (AEHNL. S.R. LEAST)
C
            WRITE (20,FMT=9000) M,N
            WRITE (20,FMT=9010) SUMF,SUMG
            WRITE (20,FMT=9030)
            DO 120 K = 1,N
               SUM = 0.D0
               DO 130 I = 1,M
                  SUM = SUM + W(I)* (Y(I)-FPL(I))*DF(I,K)
  130          CONTINUE
               SUM = -2.D0*SUM
               DIF = SQRT(AM(K,K)*SUMF)
               dpar(k)=dif
               WRITE (20,FMT=9040) K,PGUES(K),PAR(K),DIF,SUM
  120       CONTINUE
            WRITE (20,FMT=9050)
            DO 140 I = 1,N
               WRITE (20,FMT=9060) I, (RHO(I,K),K=1,N)
  140       CONTINUE
            WRITE (20,FMT=9070)
            DO 150 I = 1,M
               DIF = Y(I) - FPL(I)
               WRITE (20,FMT=9080) I,W(I),X(I),Y(I),FPL(I),DIF
  150       CONTINUE

         ELSE
            WRITE (20,FMT=9020) NR
            return

         END IF

      END IF
C
C
C     F O R M A T E
C
 9000 FORMAT ('1',/,/,/,1X,'S.R. KOVA',/,/,' THIS PROBLEM CONTAINS',I4,
     *       ' DATA POINTS AND',I3,' PARAMETERS.')
 9010 FORMAT (1X,'THE WEIGHTED VARIANCE IS',D14.7,' (CHI-SQUARE:',
     *       ' SS/(M-N))',/,' AND THE UNWEIGHTED',
     *       ' SUM OF SQUARES OF THE DEVIATIONS IS',D14.7,'.')
 9020 FORMAT (1X,'***** FEHLER: S.R. MA10AD (NR =',I5,')')
 9030 FORMAT (/,/,/,6X,'GUESSTIMATE OF   FINAL VALUE OF      S.D.  OF',
     *       /,2X,'K',3 (3X,'K-TH PARAMETER'),6X,'GRADIENT',/)
 9040 FORMAT (I3,4D17.7)
 9050 FORMAT (/,/,1X,'MATRIX OF CORRELATIONS BETWEEN FREE PARAMETERS',/)
 9060 FORMAT (/,3X,I2,10F8.3)
 9070 FORMAT (/,/,/,28X,'INDEPENDENT',6X,'DEPENDENT',7X,'CALCULATED',/,
     *       5X,'I',6X,'WEIGHT',1X,2 (9X,'VARIABLE'),9X,'FUNCTION',8X,
     *       'DEVIATION')
 9080 FORMAT (/,1X,I4,5D17.7)
 9090 FORMAT (1X,'S.R. KOVA: FEHLER( M ODER N UNZULAESSIG)',/,1X,'M = ',
     *       I10,3X,'N =',I10)

      END


* *******************************************************************
* COPYRIGHT (c) 1963 Hyprotech UK
* All rights reserved.
*
* None of the comments in this Copyright notice between the lines
* of asterisks shall be removed or altered in any way.
*
* This Package is intended for compilation without modification,
* so most of the embedded comments have been removed.
*
* ALL USE IS SUBJECT TO LICENCE. For full details of an HSL ARCHIVE
* Licence, see http://hsl.rl.ac.uk/archive/cou.html
*
* Please note that for an HSL ARCHIVE Licence:
*
* 1. The Package must not be copied for use by any other person.
*    Supply of any part of the library by the Licensee to a third party
*    shall be subject to prior written agreement between AEA
*    Hyprotech UK Limited and the Licensee on suitable terms and
*    conditions, which will include financial conditions.
* 2. All information on the Package is provided to the Licensee on the
*    understanding that the details thereof are confidential.
* 3. All publications issued by the Licensee that include results obtained
*    with the help of one or more of the Packages shall acknowledge the
*    use of the Packages. The Licensee will notify the Numerical Analysis
*    Group at Rutherford Appleton Laboratory of any such publication.
* 4. The Packages may be modified by or on behalf of the Licensee
*    for such use in research applications but at no time shall such
*    Packages or modifications thereof become the property of the
*    Licensee. The Licensee shall make available free of charge to the
*    copyright holder for any purpose all information relating to
*    any modification.
* 5. Neither CCLRC nor Hyprotech UK Limited shall be liable for any
*    direct or consequential loss or damage whatsoever arising out of
*    the use of Packages by the Licensee.
* *******************************************************************
*
*######DATE 1 Dec 1992
C       Toolpack tool decs employed.
C       ZERO and ONE made PARAMETERs.
C       U1 and U2 added.
C
      SUBROUTINE MA01BD(A,B,M,N,M1,IA,IB)
      DOUBLE PRECISION ZERO,ONE
      PARAMETER (ZERO=0.0D0,ONE=1.0D0)
      INTEGER IA,IB,M,M1,N
      DOUBLE PRECISION A(IA,M),B(IB,N)
      DOUBLE PRECISION AMAX,RAJJ,SCPROD,STO,U,UM,UM1,U1,U2,W
      INTEGER I,I1,I4,IPROD,ISTO,J,J1,K
      DOUBLE PRECISION C(300)
      INTEGER IND(300)
      EXTERNAL ZE01AD
      INTRINSIC DABS
      CALL ZE01AD(U1,U,U2,UM)
      IF (M1) 180,10,10
   10 CONTINUE
      AMAX = 0.0D0
      DO 20 I = 1,M
        IND(I) = I
        IF (DABS(A(I,1)).GT.AMAX) THEN
          AMAX = DABS(A(I,1))
          I4 = I
        END IF
   20 CONTINUE
      UM = AMAX
      DO 140 J = 1,M
        UM1 = UM
        IF (I4.GT.J) THEN
          ISTO = IND(J)
          IND(J) = IND(I4)
          IND(I4) = ISTO
          DO 30 K = 1,M
            STO = A(I4,K)
            A(I4,K) = A(J,K)
            A(J,K) = STO
   30     CONTINUE
          IF (N.GT.0) THEN
            DO 40 K = 1,N
              STO = B(I4,K)
              B(I4,K) = B(J,K)
              B(J,K) = STO
   40       CONTINUE
          END IF
        END IF
        AMAX = 0.0D0
        IF (A(J,J).EQ.ZERO) THEN
          A(J,J) = UM*U
        END IF
        UM = 0.0D0
        IF (J.GT.1) THEN
          IF (N.GT.0) THEN
            DO 60 I = 1,N
              SCPROD = ZERO
              DO 50 IPROD = 1,J - 1
                SCPROD = SCPROD + A(J,IPROD)*B(IPROD,I)
   50         CONTINUE
              B(J,I) = B(J,I) - SCPROD
   60       CONTINUE
          END IF
          IF (J.LT.M) THEN
CIBMD IGNORE RECRDEPS
            DO 80 I = J + 1,M
              SCPROD = ZERO
              DO 70 IPROD = 1,J - 1
                SCPROD = SCPROD + A(J,IPROD)*A(IPROD,I)
   70         CONTINUE
              A(J,I) = A(J,I) - SCPROD
   80       CONTINUE
          END IF
        END IF
        IF (J.LT.M) THEN
          DO 90 I = J + 1,M
            IF (DABS(A(I,J+1)).GT.UM) THEN
              UM = DABS(A(I,J+1))
            END IF
   90     CONTINUE
          RAJJ = ONE/A(J,J)
          DO 100 I = J + 1,M
            A(I,J) = RAJJ*A(I,J)
  100     CONTINUE
CIBMD IGNORE RECRDEPS
          DO 120 I = J + 1,M
            SCPROD = ZERO
            DO 110 IPROD = 1,J
              SCPROD = SCPROD + A(I,IPROD)*A(IPROD,J+1)
  110       CONTINUE
            A(I,J+1) = A(I,J+1) - SCPROD
  120     CONTINUE
          DO 130 I = J + 1,M
            IF (DABS(A(I,J+1)).GT.AMAX) THEN
              AMAX = DABS(A(I,J+1))
              I4 = I
            END IF
  130     CONTINUE
          IF (AMAX.EQ.ZERO) THEN
            IF (UM.EQ.ZERO) THEN
              UM = UM1
            END IF
          END IF
        END IF
  140 CONTINUE
      IF (N.GT.0) THEN
        DO 170 J = 1,N
          B(M,J) = B(M,J)/A(M,M)
          DO 160 I1 = 2,M
            I = M + 1 - I1
            SCPROD = ZERO
            DO 150 IPROD = I + 1,M
              SCPROD = SCPROD + A(I,IPROD)*B(IPROD,J)
  150       CONTINUE
            B(I,J) = (B(I,J)-SCPROD)/A(I,I)
  160     CONTINUE
  170   CONTINUE
      END IF
      IF (M1) 310,310,180
  180 CONTINUE
      DO 220 I1 = 1,M - 1
        I = M + 1 - I1
        C(I-1) = -A(I,I-1)
        DO 200 J1 = 2,I - 1
          J = I - J1
          SCPROD = A(I,J)
          DO 190 IPROD = J + 1,I - 1
            SCPROD = SCPROD + A(IPROD,J)*C(IPROD)
  190     CONTINUE
          C(J) = -SCPROD
  200   CONTINUE
        DO 210 K = 1,I - 1
          A(I,K) = C(K)
  210   CONTINUE
  220 CONTINUE
      A(M,M) = ONE/A(M,M)
      W = A(M,M)
      DO 230 J = 1,M - 1
        A(M,J) = W*A(M,J)
  230 CONTINUE
      DO 270 I1 = 2,M
        I = M + 1 - I1
        W = ONE/A(I,I)
        DO 250 J = 1,M
          IF (I.LT.J) THEN
            C(J) = ZERO
          ELSE IF (I.EQ.J) THEN
            C(J) = ONE
          ELSE
            C(J) = A(I,J)
          END IF
          SCPROD = ZERO
          DO 240 IPROD = I + 1,M
            SCPROD = SCPROD + A(I,IPROD)*A(IPROD,J)
  240     CONTINUE
          C(J) = C(J) - SCPROD
  250   CONTINUE
        DO 260 J = 1,M
          A(I,J) = W*C(J)
  260   CONTINUE
  270 CONTINUE
      DO 300 I = 1,M
  280   CONTINUE
        IF (IND(I).NE.I) THEN
          J = IND(I)
          DO 290 K = 1,M
            STO = A(K,I)
            A(K,I) = A(K,J)
            A(K,J) = STO
  290     CONTINUE
          ISTO = IND(J)
          IND(J) = J
          IND(I) = ISTO
          GO TO 280
        END IF
  300 CONTINUE
  310 CONTINUE
      RETURN
      END


* *******************************************************************
* COPYRIGHT (c) 1966 Hyprotech UK
* All rights reserved.
*
* None of the comments in this Copyright notice between the lines
* of asterisks shall be removed or altered in any way.
*
* This Package is intended for compilation without modification,
* so most of the embedded comments have been removed.
*
* ALL USE IS SUBJECT TO LICENCE. For full details of an HSL ARCHIVE
* Licence, see http://hsl.rl.ac.uk/archive/cou.html
*
* Please note that for an HSL ARCHIVE Licence:
*
* 1. The Package must not be copied for use by any other person.
*    Supply of any part of the library by the Licensee to a third party
*    shall be subject to prior written agreement between AEA
*    Hyprotech UK Limited and the Licensee on suitable terms and
*    conditions, which will include financial conditions.
* 2. All information on the Package is provided to the Licensee on the
*    understanding that the details thereof are confidential.
* 3. All publications issued by the Licensee that include results obtained
*    with the help of one or more of the Packages shall acknowledge the
*    use of the Packages. The Licensee will notify the Numerical Analysis
*    Group at Rutherford Appleton Laboratory of any such publication.
* 4. The Packages may be modified by or on behalf of the Licensee
*    for such use in research applications but at no time shall such
*    Packages or modifications thereof become the property of the
*    Licensee. The Licensee shall make available free of charge to the
*    copyright holder for any purpose all information relating to
*    any modification.
* 5. Neither CCLRC nor Hyprotech UK Limited shall be liable for any
*    direct or consequential loss or damage whatsoever arising out of
*    the use of Packages by the Licensee.
* *******************************************************************
*
*######DATE 9 Feb 1993
C       Toolpack tool decs employed.
C
      SUBROUTINE ZE01AD(A,B,C,D)
      DOUBLE PRECISION A,B,C,D
      DOUBLE PRECISION EA,EB,EC,ED,P,Q,R,S,X,Y
      INTEGER I,K
      INTRINSIC DABS,DMAX1
      X = 1.D00 + 1./9.
      Y = 1.0D00 + 1./12.
      K = 1
      P = 1.0D0/3.0D0
      Q = P
      EA = 0.0D0
      EB = 0.0D0
      EC = 0.0D0
      ED = 0.0D0
      DO 1 I = 1,100
        P = P*X
        Q = Q*Y
        R = P + Q
    2   S = (R-P-Q)/R
        EA = DMAX1(EA,DABS(S))
        EB = EB + DABS(S)
        EC = EC + S
        R = P*Q
    3   S = (R/P-Q)/Q
        EA = DMAX1(EA,DABS(S))
        EB = EB + DABS(S)
        ED = ED + S
        R = 1.0D0/P
        S = 1.0D0/Q
    4   EA = DMAX1(EA,DABS(P*R-1.),DABS(Q*S-1.))
        GO TO (1,2,3,4) K
    1 CONTINUE
      A = EA
      B = EB/200.0D0
      C = EC/100.0D0
      D = ED/100.0D0
      RETURN
      END


