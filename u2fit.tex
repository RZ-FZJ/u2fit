\documentstyle[12pt,environ,amsmath]{article}

\textwidth 160mm
\textheight 247mm
\hoffset -15mm
\voffset -20mm
\parindent=0pt
\parskip=6pt
\sloppy

\def\bb{\rule{24pt}{8.2pt}}
\def\bc{\rule{5.875pt}{7.333pt}}
\def\em{\bf}
\def\d{{\mathrm d}}
\def\eq#1{(\ref{#1})}
\def\d{{\rm d}}
\def\e{{\rm e}}
\def\I{{\rm i}}
\def\kB{k_{\rm B}}
\def\AArez{{\,\mbox{\AA}^{-1}}}
\def\Ar{{\AA$^{-1}$}}
\def\FT#1#2{{\cal F}_{#1|\,#2}}
\def\iFT#1#2{{\cal F}^{-1}_{#1|\,#2}}
\def\<{\langle}
\def\>{\rangle}

\NewEnviron{eqs}{%
\begin{equation}
  \begin{split}
    \BODY
  \end{split}
\end{equation}}
\NewEnviron{eqm}{%
\begin{multline}
  \BODY
\end{multline}}

\def\uu{{\<u^2\>}}
\def\iel{I_{\rm el}}

\begin{document}


\begin{center}
{\LARGE\bf u2fit (open version)}\\[5mm]
{Version of \today}\\[10mm]
{\it Reiner Zorn}\\[20mm]
\end{center}

\section{Purpose and Operation}\label{purpose}
The open version of u2fit is written for testing out different models for describing elastic scans. A model is specified by a function $\iel(Q)$ containing a couple of parameters. In practice, not more than three free parameters are possible. Usually, the first of these is the mean-square displacement, $\uu$. The model is applied to each data set (temperature) in the elastic scan separately. The `standard treatment' is always performed before starting the fit:
\begin{itemize}
\item Data sets for all temperatures are divided by the one at the lowest temperatures. So it is actually $\iel(Q,T)/\iel(Q,T_{\rm min})$ which is fitted.
\item A couple of data sets in the beginning of the scan can be averaged to improve the statistics of $\iel(Q,T_{\rm min})$.
\item Multiple scans can be concatenated from separate files.
\item A detector range to be fitted can be selected. This is important to avoid low $Q$ detectors with worse resolution.
\item A maximum temperature can be selected for models which do not apply at high temperatures.
\end{itemize}

The file to be loaded has to be in ILL's CROSSX/INX/SQW format. (In the recent version, the format requirement is `relaxed'. Numbers may be separated by arbitrary length white space.) Apart from the data itself, it must contain correct values of the scattering angles (or $Q$ values) and the incident energy (from which the wave length is calculated).

After the fit has been done, an aggregate $\chi^2$ is given. (The individual sums of squares for each temperature are written to the file {\tt fort.99}.) This should not be taken too seriously. It is more instructive to use the next following option to plot a single data set at a certain temperature. The plot shows the data, the fit function, and in some cases components as the multiple scattering or the background.

For some models multiple scattering can be added. A global multiple scattering probability has to be given by the user. Strictly speaking, this is the probability for second scattering which is assumed to be the conditional probability for all higher order scattering events too. This value will be applied equally to all temperatures. The fit function will be corrected, not the data. It is possible to refine this parameter automatically. But it has to be noted that this only makes sense if the model function describes the data correctly in the temperature range used. Otherwise, features not included in the model may be misinterpreted as multiple scattering.

Finally, u2fit provides a file where each line contains the temperature and for each parameter its fitted value and error estimate. The filename will be {\tt u2\_m\it xx\tt \_s\it yy\tt .dat} where {\it xx} is the model number and {0.\it yy} is the global multiple scattering probability. The subroutine {\tt KOVA} which calculates the error estimates produces extensive diagnostic output which will be recorded in the file {\tt fort.20}. If selected by the parameter {\tt iprint} also the fit routine itself, {\tt VA05AD}, produces a log in file {\tt fort.21}.

\section{Compilation}
Compile the code with the command
\begin{verbatim}
gfortran -std=legacy -w -ffixed-line-length-none -o u2fit u2fit_open.f
\end{verbatim}
or using a similar FORTRAN compiler. Note that the options will suppress all errors and warnings which arise from the HSL library code but could also hide potential other errors.

%Probably there will  be a warning like
%\begin{verbatim}
%      CALL CALFUN (M,N,F,X)
%                       1
%Warning: Actual argument contains too few elements for dummy argument 
%'f' (1/50) at (1)
%\end{verbatim}
%This warning is due to the programming techniques used in the fit subroutine {\tt VA05} of the HSL library but can be ignored.

\section{Workflow}
The following inputs will be required from the user.

\begin{enumerate}
\item {\em Elastic scan file (SQW type) name} The input file has to conform with the output format of the ILL program SQW, known as CROSSX or INX format.

\item{\em Constant Q?} This tells u2fit whether to treat the value in the INX file as angle (default) or $Q$ value.

\item {\em Average first n temperatures (0:none)} To increase the accuracy of the normalisation of the scan the lowest temperature may have been measured several times and can be averaged here. Input of 0 or 1 normalises to the first point.

\item {\em Another elastic scan file (SQW type) or ""} Additional files can be loaded if the scan was measured in separate parts.

\item {\em Detector range for fit} Here, obviously bad detectors can be excluded. Currently, only `first last' is possible giving the range of detectors to be used.

\item {\em Highest temperature to fit (0:all)} If a model is expected to break down at high temperatures these can be excluded from the fit here.

\item {\em Available Models: \dots Select} Here the fit model is selected. Refer to section~\ref{functions}.

\item {\em Use parameter propagation (y/N)} Use fitted parameters of a temperature as start values for the next. See section~\ref{propreg}.

\item {\em Regularise (parameter number, multiplier)} If regularisation is desired (section~\ref{propreg}) enter the parameter number and the regularisation multiplier. Otherwise enter blank.

\item (only for models with global multiple scattering) {\em Multiple scattering fraction} Enter the multiple scattering fraction $s$ of equation~\eq{mscorr} or a reasonable guess, e.g. 0.1.

\item (only for models with global multiple scattering) {\em Optimize MS fraction (y/N)} If y is entered the multiple scattering fraction $s$ is optimised such that the fit's $\chi^2$ gets minimal.

\end{enumerate}

At that point the fit will be carried out and the reduced $\chi^2$ printed. You can now inspect the fits visually.

\begin{enumerate}
\setcounter{enumi}{10}
	
\item {\em Plot fit result for temperature (0: end)} Enter the temperature to inspect. For the nearest step $I_{\rm el}(T)/I_{\rm el}(T_{\min})$ will be plotted logarithmically vs $Q^2$. Enter 0 to finish inspection.

\item {\em How to proceed? \dots} 0 and 1 exit without or with saving the result, respectively. 2 restarts from step 4 with the same scan data being used. 3 restarts but saves the result from the fit so that it can be copied or renamed to keep it from being overwritten.

\end{enumerate}

If 1 or 3 were selected an output file as described in section~\ref{purpose} will be written.

\section{Installed Fit Functions}\label{functions}
The following functions are already implemented. If you want to create an own function please read this section before to learn about standards. Most importantly, to ensure compatibility, use {\tt par(1)} for $\uu$ and use the definition based on $ \exp \left( - \uu Q^2 / 3 \right)$.

\subsection{Gaussian}
This is the `standard' treatment of elastic scans based on
\begin{equation}\label{gaussian}
  \iel(Q) = I_0 \exp \left( - {\uu Q^2 \over 3} \right) \,.
\end{equation}
Note that if there is no irregularity as sample loss or beam intensity fluctuation there is no reason for $I_0 \neq 1$. In practice, $I_0$ will be smaller than one due to multiple scattering.

\subsection{Non-Gaussian}
This is the simplest treatment of curvature in the $\ln \iel$ vs $Q^2$ plot:
\begin{equation}\label{nongaussian}
  \iel(Q) = I_0 \exp \left( - {\uu Q^2 \over 3} + {\alpha_2 \uu^2 Q^4 \over 18} \right) \,.
\end{equation}
The second term is written in a way that $\alpha_2$ has the meaning of a non-Gaussianity parameter. Nevertheless, the curvature will nearly always be in part due to multiple scattering and broadening of the central line due to diffusion. So it should not be immediately interpreted in physical terms.

\subsection{Gaussian without intensity factor}\label{gauss}
This is the same expression as \eq{gaussian} but with $I_0=1$, thus having one fit parameter less. As a more realistic explanation of $\iel (0) < 1$ a global multiple scattering probability $s$ can be introduced. In that case the average elastic intensity will be calculated as
\begin{equation}\label{msgauss}
  \overline{\iel} = { 2 \over Q_{\rm max}^2 } 
  \int_0^{Q_{\rm max}} \iel(Q) Q \d Q
  = { 3 \over \uu Q_{\rm max}^2 } \left( 1- \exp \left( - { \uu Q_{\rm max}^2 \over 3 } \right)  \right)
\end{equation}
where $ Q_{\rm max} = 2 k $ is the maximal $Q$ value (i.e.\ for $180^\circ$ scattering angle).
Subsequently each elastic intensity will be corrected according to
\begin{equation}\label{mscorr}
  \iel^{\rm ms} (Q) = (1-s) \left( \iel(Q) + { s {\overline{\iel}}^2 \over 1 - s {\overline{\iel}} } \right) \,.
\end{equation}
For all models containing multiple scattering the multiple scattering level will be indicated in the detectors' plots. This is done in the subroutine {\tt plotdet} if either a value of $\overline{\iel}>0$ was calculated or the model function contains an individual multiple scattering component. This mechanism can be used for any model to include separate terms in the plot.

\subsection{Non-Gaussian without intensity factor}\label{nongauss}
This is the same expression as \eq{nongaussian} but with $I_0=1$, thus having one fit parameter less. Also here, global multiple scattering $s$ can be used. Although the integral \eq{msgauss} could be calculated analytically, the resulting expression would be so complex that numerical integration as
\begin{equation}\label{msint}
  \overline{\iel} = { 1 \over Q_{\rm max}^2 } \int_0^{Q_{\rm max}^2} \iel(Q) \d Q^2
\end{equation}
is preferred.

Because this calculation is rather computing-intensive it is only done once for each temperature (not each $Q$). For that purpose, the code is placed in subroutine {\tt initt}. This can be done for any calculation which depends only on the fit parameters but not on $Q$. The results of such a calculation have to be put into the common {\tt /funpar/} to be available in the fit function. For the calculation of the integral \eq{msint} the integrand is identical to $\iel(Q)$ itself. Therefore it makes sense to define an additional function {\tt fngauss} for that, so that any possible changes of the function will also be effective in the integral. It is recommended to proceed like this for any $\iel(Q)$ which should be corrected for multiple scattering numerically.

\subsection{Gaussian with individual multiple scattering}
This is the same as \ref{gauss} but with $s$ being an individual fit parameter. This is in priciple `unphysical' because the multiple scattering probability should not change with temperature. For low temperatures it is also impossible to be fitted because $\iel(0)$ does not sufficiently differ from one. For high temperatures the diffusion-broadening effect will misinterpreted in terms of a higher $s$. Nevertheless, it may be a good way to find out the temperature range from which $s$ can be determined.

\subsection{Diffusion (Gaussian resolution)}\label{diff}
This model is based on quasielastic broadening from pure simple diffusion. For a Gaussian-shaped instrumental resolution the apparent elastic intensity is
\begin{equation}\label{diffusion}
  \iel(Q) = \exp\left( {\pi \over 36} \uu^2 Q^4 \right) {\rm erfc}\left( {\sqrt{\pi} \over 6} \uu Q^2 \right) \,.
\end{equation}
The apparent $\uu$ is related to the diffusion coefficient by $ D = \sqrt{\pi/18} \uu \sigma $, where $\sigma$ is the (Gaussian) width of the resolution in units of angular frequency ($\Delta\omega$). Note that for fitting functions like this detectors have to be excluded which have a different resolution because u2fit does not provide a detector-dependent $\sigma$.

\subsection{Diffusion + fast motion (Gaussian resolution)}\label{difff}
This model is more realistic than the one before, because even in the presence of diffusion there will be fast motions. Assuming that these contribute in a Gaussian fashion, one obtains a combination of \ref{gauss} and \ref{diff}:
\begin{equation}
  \iel(Q) = \exp \left( - {\uu_{\rm fast} Q^2 \over 3} + {\pi \over 36} \uu_{\rm diff}^2 Q^4 \right) {\rm erfc}\left( {\sqrt{\pi} \over 6} \uu_{\rm diff} Q^2 \right) \,.
\end{equation}
To be consistent with the preceding model functions, the fitted parameters are $\uu = \uu_{\rm fast} + \uu_{\rm diff}$ and $\uu_{\rm fast}$.

\subsection{Gaussian + sloped background}
This model is based on the assumption that the curvature of the in the $\ln \iel$ vs $Q^2$ plot is due to a background created by inelastically scattered neutrons reaching the detector directly which are so slow that they escape the detector gating. It is assumed that this background increases $\propto Q_{\rm max} - Q^2$ which is the squared scattering vector for direct scattering on a backscattering spectrometer:
\begin{equation}
  \iel(Q) = I_0 \exp \left( - {\uu Q^2 \over 3} \right) + f_{\rm inel} \left( Q_{\rm max} - Q^2 \right) \,.
\end{equation}
Because the inelastic scattering is temperature dependent an individual fit of $f_{\rm inel}$ is necessary.
%Up to the order of $Q^4$ this expression is identical with \eq{nongaussian} just with different parameters. This also holds for $\uu$, which is higher by $ 3 f_{\rm inel} / I_0 $ if fitted by this expression.

\subsection{Gaussian without intensity factor + sloped background}
This is the combination of \ref{gauss} with a sloped background as before. The fact that $\iel (0) < 1$ has to be taken into account by multiple scattering. The correction \eq{mscorr} is applied only to the non-background part. For the background the multiple-scattering-corrected version would have to be something like $ f_1 + f_2 \left( Q_{\rm max} - Q^2 \right) $. But it is impossible to fit one more parameter or to derive the dependence between $f_1$ and $f_2$ without additional knowledge of the fast processes. Therefore, the inelastic multiple scattering component of the background is neglected.

\subsection{Gaussian without intensity factor + flat background}
This is similar to the preceding model, just with a flat background:
\begin{equation}
  \iel(Q) = I_0 \exp \left( - {\uu Q^2 \over 3} \right) + I_{\rm bkg} \,.
\end{equation}
In principle, there is no reason why the background should be flat in $Q$. But it may be the best guess if one does not know where the background comes from.

\subsection{Ad hoc model with three parameters}
A template for implementing a model with three parameters without explicit multiple scattering treatment.

\subsection{Ad hoc model with two parameters}
A template for implementing a model with two parameters with multiple scattering treatment by equations \eq{mscorr} and \eq{msint}.

\subsection{The Magic function}
\begin{equation}\label{magic}
%  \iel(Q) = \exp \left( - { a Q^2 \Delta \over 1 + a Q^2 } \right) \,.
  \iel(Q) = \exp \left( - { \uu Q^2 / 3 \over 1 + \alpha_2 \uu Q^2 / 6 } \right) \,.
\end{equation}
This function gives an excellent fit when combined with global multiple scattering (at least for SPHERES data). It fits also the low-$Q$ detectors well. In addition, a free fit of the multiple scattering parameter over the whole temperature range does not lead to the misinterpretation of the curvature usually found for other models. Unfortunately, there is no physical reason for this functional form. Nevertheless, it may be seen as an improvement of the model in \ref{nongauss} avoiding the upturn due to the $Q^4$ term in the latter. Instead, the function decreases monotonously to $\exp(-2/\alpha_2)$ in the limit of large $Q$.

On the first glance, a physical justification could be seen in the Singwi-S\"olander model giving the intermediate scattering function as
\begin{equation}
  S(Q,t) = \exp \left( - {t\over\tau} { Q^2 {r_0}^2 \over 1 +  Q^2 {r_0}^2 } \right)
\end{equation}
which has the same $Q$ dependence as \eq{magic} with the correspondence $ \uu = 3 ( t / \tau) {r_0}^2 $, $ \alpha_2 = 2 / ( t / \tau ) $. But despite the similarity it is not possible to identify $\iel(Q)$ with the value of $S(Q,t)$ at a certain time $t$. Indeed a calculation of the Singwi-S\"olander model with a Lorentzian resolution of HWHM $\sigma$ gives an algebraic expression:
\begin{equation}
  \iel(Q) = { 1 \over { 1 + { 1 \over \tau\sigma} { Q^2 {r_0}^2 \over 1 +  Q^2 {r_0}^2 } }}
\end{equation}
(not yet implemented). The calculation for a Gaussian resolution is possible but more complicated.

\subsection{Diffusion (Loretzian resolution)}\label{diffl}
[Not yet implemented] This is the variant of model \ref{diff} with a Lorentzian resolution of HWHM $\sigma$ with the particularly simple result
\begin{equation}\label{diffusion}
	\iel(Q) = { 1 \over 1 + \uu Q^2 / 3 } \,.
\end{equation}
The apparent $\uu$ is related to the diffusion coefficient by $ D = \uu \sigma / 3 $.

\subsection{Diffusion + fast motion (Lorentzian resolution)}
[Not yet implemented] This is the variant of model \ref{difff} with a Lorentzian resolution of HWHM $\sigma$:
\begin{equation}
	\iel(Q) = { \exp( - \uu_{\rm fast} Q^2 / 3) \over 1 + \uu Q^2 / 3 }  \,.
\end{equation}


%The relation of the parameters $a$ and $\Delta$ to those in \eq{nongaussian} are
%\begin{equation}
%  \uu = 3 a \Delta , \qquad \alpha_2 = 2/\Delta \,.
%\end{equation}
%The latter parameters are also those which are fitted and $a$ and $\Delta$

\section{Parameter propagation, regularisation}\label{propreg}
Stability issues may arise for models with two or more parameters. One way to improve the chances to find the `right' minimum is the use of the parameters from the last fitted temperature as start values of the actual, parameter propagation. Unfortunately, this may cause the sequence of fits to `derail' if there is one fit with outlier parameters which do not work as start parameters for the next.

A more severe intervention in the fit process is regularisation: An additional `penalty' is added to the sum of squares favouring smaller fluctuations of one or more parameters. Here, this is done in a crude way by adding $ \lambda \left( p_i - p_{i-1} \right) $ to the sum of squares where $p_i$ is the parameter to be fitted, $p_{i-1}$ the parameter from the last fitted temperature, and $\lambda$ the regularisation multiplier. Its choice is somewhat arbitrary. Too small values have no effect but too large values flatten out the temperature dependence artificially. For the usual situation of $\chi^2$ close to one and parameters with values around one, $\lambda=10$ is a good first guess. This can be applied to all parameters, but it is recommended to do so only for `hidden' parameters like $\alpha_2$ and not for $\uu$ itself. Nevertheless, the damping effect on the other parameter(s) will also reduce the fluctuations of $\uu$.

\section{How to implement a model}
If a model is going to be tested on the spot and fits into one of the following two categories, it can be done by (1) modifying the code in subroutine {\tt eli} in the clause {\tt if (isfun.eq.11) then ... end if} for a three parameter model or (2) modifying function {\tt fadhoc} for two parameters and multiple scattering.

If not, or if a permanent use of a model is desired, a couple of steps have to be be taken:
\begin{enumerate}
\item Increase the number of models {\tt nfun} in {\tt dim\_open.f} by one.
\item Add the number of parameters and 0/1 depending on whether (global) multiple scattering can be used in data {\tt npar} and data {\tt ism}, respectively.
\item Add a calculation block in function {\tt eli}. You can copy and modify this block from already defined similar functions.
\item If necessary add an initialisation block in subroutine {\tt initt}. If this involves an integration to calculate multiple scattering you can copy one of the existing blocks. Note that in this case the calculation of $\iel(Q)$ before multiple scattering has to be put into a separate function, like function {\tt fngauss}. This function has to be declared external in subroutine {\tt initt}.
\item Add a fit initialisation block in subroutine {\tt fit}. Normally, you do not need to change the parameters of subroutine {\tt va05} except for {\tt iprint} which may be set to 1 for diagnostic output.
\item Optionally, if components of the fit function should be plotted, add a block to subroutine {\tt plotdet}. Warning: You need some knowledge of the syntax of gnuplot to do so. Also it may help to have a look at the existing blocks.
\end{enumerate}

\end{document}
