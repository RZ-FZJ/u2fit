# u2fit

Data from neutron scattering elastic scans are read and fitted with models. The fits are performed individually for the temperatures yielding parameters as the mean-squared dispalcement or diffusion constant. Multiple scattering can be taken into account. In addition to the pre-installed models the user may define extra models.
